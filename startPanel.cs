﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Economator.Editar;
using EconomatorAppService;
using EconomatorAppService.DTOs;
using EconomatorAppService.DTOsTy2;



namespace Economator
{
    public partial class startPanel : Form
    { //TODO: Hay que poner un selector en la fecha que permita eliger entre fecha y fecha valor

        public startPanel()
        {
            InitializeComponent();
        }


        private void startPanel_Load(object sender, EventArgs e)
        {

        }

        #region Fechas / Cuenta / Linea de cuenta / Entidad
        public DateTime getDesde_DatosOrigen()
        {
            DateTime desde = dateTimePicker1.Value;
            return desde;
        }
        public DateTime getHasta_DatosOrigen()
        {
            DateTime desde = dateTimePicker2.Value;
            return desde;
        }
        public string getCuenta_DatosOrigen()
        {
            string cuenta = null;
            try
            {
                //cuenta = comboBox2.SelectedItem.ToString();
                cuenta = toolStripComboBox2.SelectedItem.ToString();
            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx, "No as seleccionado ninguna Cuenta");
            }
            return cuenta;
        }
        public string getLineaCuenta_DatosOrigen()
        {
            string cuenta = null;
            try
            {
                cuenta = toolStripComboBox1.SelectedItem.ToString();
            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx, "No as seleccionado ninguna Cuenta");
            }
            return cuenta;
        }
        public string getEntidad_DatosOrigen()
        {
            string cuenta = null;
            try
            {
                cuenta = toolStripComboBox3.SelectedItem.ToString();
            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx, "No as seleccionado ninguna Cuenta");
            }
            return cuenta;
        }

        public DateTime getDesde_Main()
        {
            DateTime desde = dateTimePicker5.Value;
            return desde;
        }
        public DateTime getHasta_Main()
        {
            DateTime desde = dateTimePicker6.Value;
            return desde;
        }
        public string getCuenta_Main()
        {
            string cuenta = null;
            try
            {
                cuenta = comboBox11.SelectedItem.ToString();
            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx, "No as seleccionado ninguna Cuenta");
            }
            return cuenta;
        }
        public string getLineaCuenta_Main()
        {
            string cuenta = null;
            try
            {
                cuenta = comboBox10.SelectedItem.ToString();
            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx, "No as seleccionado ninguna Cuenta");
            }
            return cuenta;
        }

        public DateTime getDesde_ControlGasto()
        {
            DateTime desde = dateTimePicker4.Value;
            return desde;
        }
        public DateTime getHasta_ControlGasto()
        {
            DateTime desde = dateTimePicker3.Value;
            return desde;
        }
        public string getCuenta_ControlGasto()
        {
            string cuenta = null;
            try
            {
                cuenta = comboBox9.SelectedItem.ToString();
            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx, "No as seleccionado ninguna Cuenta");
            }
            return cuenta;
        }
        public string getLineaCuenta_ControlGasto()
        {
            string cuenta = null;
            try
            {
                cuenta = comboBox8.SelectedItem.ToString();
            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx, "No as seleccionado ninguna Cuenta");
            }
            return cuenta;
        }
        #endregion

        #region Ventanas
        private void CargarFicheroDeDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarFichero cf = new cargarFichero();
            cf.Show();
        }
        private void EditarCuentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editaCuentas ec = new editaCuentas();
            ec.Show();
        }
        private void ConceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editaConceptos ec = new editaConceptos();
            ec.Show();
        }
        private void GruposDeConceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editaGrupo eg = new editaGrupo();
            eg.Show();
        }
        private void PresupuestosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editaPresupuestos ep = new editaPresupuestos();
            ep.Show();
        }
        private void opcionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Opciones opciones = new Opciones();
            opciones.Show();
        }
        #endregion

        #region Pestaña Principal
        private void ComboBox4_DropDown(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                comboBox4.Items.Clear();
                foreach (var reg in easa.getListGroupConcepts())
                {
                    comboBox4.Items.Add(reg);
                }
            }

        }
        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                ChartParams_DTO chP = new ChartParams_DTO();
                chP.Grafica = chart1;
                chP.TypeChart = comboBox7.SelectedItem.ToString();
                chP.grpConcepts = comboBox4.SelectedItem.ToString();
                chP.year = Convert.ToInt32(comboBox5.SelectedItem.ToString());
                chP.SerieName = "Gasto Anual";
                chP.presupuesto = checkBox7.Checked;
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    easa.datosGrupos1Yr(chP);
                }

            }
            catch (NullReferenceException nrEx)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, nrEx);
            }

        }
        #endregion

        #region pestaña Control de Gasto
        private void ComboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                foreach (var reg in easa.getDescripcionTiempo(getDesde_ControlGasto(), getHasta_ControlGasto()))
                {
                    comboBox1.Items.Add(reg);
                }
            }
        }
        private void ComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            label2.Text = "Gasto total: ";
            label6.Text = "Numero de operaciones: ";
            string descripcion = comboBox1.SelectedItem.ToString();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                label2.Text = label2.Text + easa.getGastoConceptoTiempo(getDesde_ControlGasto(), getHasta_ControlGasto(), descripcion) + '€';
                label6.Text = label6.Text + easa.getNumOpsConceptoTiempo(getDesde_ControlGasto(), getHasta_ControlGasto(), descripcion);
                var lstOps = easa.getListaOpsConcepto(getDesde_ControlGasto(), getHasta_ControlGasto(), descripcion).ToList();
                listView1.Items.Clear();
                foreach (var reg in lstOps)
                {
                    listView1.Items.Add(reg.fecha.ToString("dd/MM/yyyy") + " | " + reg.descripcion + " | " + reg.importe + " €");
                }
            }

        }
        private void ComboBox3_DropDown(object sender, EventArgs e)
        {

            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                comboBox3.Items.Clear();
                foreach (var reg in easa.getListGroupConcepts())
                {
                    comboBox3.Items.Add(reg);

                }
            }

        }
        private void ComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string grupo = comboBox3.SelectedItem.ToString();
                double totalGrupo = 0;
                listView2.Items.Clear();
                label8.Text = "Total gasto grupo: ";
                label9.Text = "Numero de operaciones: ";

                List<DatosFuenteBankia_DTO> ultimosRegs = new List<DatosFuenteBankia_DTO>();
                List<string> lstDescriGrupo = new List<string>();
                EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess();

                ultimosRegs = easa.getDatosFuente(getDesde_ControlGasto(), getHasta_ControlGasto(), getCuenta_ControlGasto());
                lstDescriGrupo = easa.getConceptosGrupo(grupo);


                for (int i = 0; i < ultimosRegs.Count; i++)
                {
                    string descri = "";

                    descri = easa.getGruposConcepto(ultimosRegs[i].descripcion).FirstOrDefault().nombreConcepto;
                    for (int j = 0; j < lstDescriGrupo.Count; j++)
                    {
                        if (descri == lstDescriGrupo[j])
                        {
                            listView2.Items.Add($@"{ultimosRegs[i].fecha.ToString("dd/MM/yyyy")} | {lstDescriGrupo[j]} | {ultimosRegs[i].importe}  €");
                            totalGrupo = totalGrupo + ultimosRegs[i].importe;
                        }
                    }

                }
                label8.Text = label8.Text + totalGrupo + " €";
                label9.Text = label9.Text + listView2.Items.Count;
            }
            catch (NullReferenceException nrEx)
            {
                // errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, nrEx);
            }
            catch (Exception ex)
            {
                // errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
        }
        private void comboBox8_DropDown(object sender, EventArgs e)
        {
            if (comboBox8.Items != null && comboBox8.Items.Count > 0)
            {
                comboBox8.Items.Clear();
            }
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                foreach (var reg in easa.getOptionsValues("LineasCuentas"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox8.Items.Add(reg);
                    }

                }
            }
        }
        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Enabled = true;
            //checkBox3.Enabled = true;
        }
        private void comboBox9_DropDown(object sender, EventArgs e)
        {
            string nomLinea = comboBox8.SelectedItem.ToString();
            List<cuentas_DTO> lst = new List<cuentas_DTO>();
            comboBox9.Items.Clear();
            if (checkBox3.Checked == true)
            {

                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    lst = easa.getCuentas().Where(x => x.LineaCuenta == nomLinea).ToList();
                }


                comboBox9.Text = $"Todas las cuentas de la linea {nomLinea}";
                comboBox9.Enabled = false;
            }
            else
            {
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    lst = easa.getCuentas().Where(x => x.LineaCuenta == nomLinea).ToList();
                }

            }
            foreach (var reg in lst)
            {
                comboBox9.Items.Add(reg.nombreCCC);
            }
        }
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                dateTimePicker4.Enabled = false;
                dateTimePicker4.Value = new DateTime(2017, 10, 01);
            }
            else
            {
                dateTimePicker4.Enabled = true;
                dateTimePicker4.Value = DateTime.Today.AddDays(-7);
            }
        }
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            string nomLinea = comboBox8.SelectedItem.ToString();
            if (String.IsNullOrEmpty(comboBox8.Text))
            {
                comboBox9.Enabled = false;
                checkBox3.Enabled = false;
            }
            else
            {
                comboBox9.Enabled = true;
                checkBox3.Enabled = true;
            }

            if (checkBox3.Checked == true)
            {
                comboBox9.Text = $"Cuentas de la linea {nomLinea}";
                comboBox9.Enabled = false;
            }
            else
            {
                comboBox9.Text = "";
            }
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox9.Enabled = true;
            checkBox3.Enabled = true;
        }
        private void comboBox2_DropDown(object sender, EventArgs e)
        {
            comboBox9.Text = String.Empty;
            if (comboBox2.Items != null && comboBox2.Items.Count > 0)
            {
                comboBox2.Items.Clear();
            }
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                foreach (var reg in easa.getOptionsValues("Entidades"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox2.Items.Add(reg);
                    }

                }
            }
        }
        #endregion




        #region Pestaña Datos Origen
        private void allCCCsCheck_CheckedChanged(object sender, EventArgs e)
        {
            string nomLinea = toolStripComboBox1.SelectedItem.ToString();
            if (String.IsNullOrEmpty(toolStripComboBox1.Text))
            {
                toolStripComboBox2.Enabled = false;
                allCCCsCheck.Enabled = false;
            }
            else
            {
                toolStripComboBox2.Enabled = true;
                allCCCsCheck.Enabled = true;
            }

            if (allCCCsCheck.Checked == true)
            {
                toolStripComboBox2.Text = $"Cuentas de la linea {nomLinea}";
                toolStripComboBox2.Enabled = false;
            }
            else
            {
                toolStripComboBox2.Text = "";
            }

        }
        private void toolStripComboBox2_DropDown(object sender, EventArgs e)
        {
            string nomLinea = toolStripComboBox1.SelectedItem.ToString();
            string entidad = toolStripComboBox3.SelectedItem.ToString();
            List<cuentas_DTO> lst = new List<cuentas_DTO>();
            toolStripComboBox2.Items.Clear();
            if (allCCCsCheck.Checked == true)
            {

                //using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                //{
                //    lst = easa.getCuentas().Where(x => x.LineaCuenta == nomLinea && x.entidadCCC == entidad).ToList();
                //}

                toolStripComboBox2.Text = $"Todas las cuentas de la linea {nomLinea}";
                toolStripComboBox2.Enabled = false;
            }
            else
            {
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    lst = easa.getCuentas().Where(x => x.LineaCuenta == nomLinea && x.entidadCCC == entidad).ToList();
                }

            }
            foreach (var reg in lst)
            {
                toolStripComboBox2.Items.Add(reg.nombreCCC);
            }
        }
        private void toolStripComboBox1_DropDown(object sender, EventArgs e)
        {
            toolStripComboBox3.Text = String.Empty;
            toolStripComboBox2.Text = String.Empty;
            if (toolStripComboBox1.Items != null && toolStripComboBox1.Items.Count > 0)
            {
                toolStripComboBox1.Items.Clear();
            }
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                foreach (var reg in easa.getOptionsValues("LineasCuentas"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        toolStripComboBox1.Items.Add(reg);
                    }

                }
            }
        }
        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolStripComboBox3.Enabled = true;
            //allCCCsCheck.Enabled = true;
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            bool todas = allCCCsCheck.Checked;
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                easa.getDatos(todas,
                              getEntidad_DatosOrigen(),
                              getDesde_DatosOrigen(),
                              getHasta_DatosOrigen(),
                              getLineaCuenta_DatosOrigen(),
                              getCuenta_DatosOrigen(),
                              dataGridView1);

                toolStripStatusLabel1.Text = dataGridView1.Rows.Count.ToString() + " registros"; ;
            }
        }
        private void toolStripComboBox3_DropDown(object sender, EventArgs e)
        {
            toolStripComboBox2.Text = String.Empty;
            if (toolStripComboBox3.Items != null && toolStripComboBox3.Items.Count > 0)
            {
                toolStripComboBox3.Items.Clear();
            }
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                foreach (var reg in easa.getOptionsValues("Entidades"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        toolStripComboBox3.Items.Add(reg);
                    }

                }
            }
        }
        private void toolStripComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolStripComboBox2.Enabled = true;
            allCCCsCheck.Enabled = true;
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Value = new DateTime(2017, 10, 01);
            }
            else
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker1.Value = DateTime.Today.AddDays(-7);
            }
        }


        #endregion

        private void tabPage4_Enter(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                foreach (var reg in easa.getListGroupConcepts())
                {
                    listBox1.Items.Add(reg);
                }
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                if (listBox1.SelectedItem != null)
                {
                    listBox2.Items.Clear();
                    foreach (var reg in easa.getConceptosGrupoDetail(listBox1.SelectedItem.ToString()))
                    {
                        listBox2.Items.Add($"{reg.nombreConcepto}|{reg.cuenta}");
                    }
                }
            }
        }

        private void comboBox6_DropDown(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
               
                    comboBox6.Items.Clear();
                    foreach (var reg in easa.getNombresPresupuesto())
                    {
                        comboBox6.Items.Add(reg);
                    }
                

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                if(listBox2.SelectedItem != null)
                {
                    easa.asignarPresupuesto(listBox2.SelectedItem.ToString(), comboBox6.Text);
                }
               
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string concept = String.Empty;
            if(listBox2.SelectedItem != null)
            {
                concept = listBox2.SelectedItem.ToString();
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    label15.Text = easa.getPresupuestoConcepto(concept);
                    label15.ForeColor = Color.Black;
                    checkBox1.Checked = true;
                    if (label15.Text == "Ninguno")
                    {
                        label15.ForeColor = Color.Red;
                        checkBox1.Checked = false;
                    }
                }
            }
          
        }
    }
}

