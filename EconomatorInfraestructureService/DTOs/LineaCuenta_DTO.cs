﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfraestructureService.DTOs
{
    public class LineaCuenta_DTO
    {
        public Guid idLineaCuenta { get; set;}
        public string NombreLineaCuenta { get; set; }

        public LineaCuenta_DTO()
        {
           
        }
        public LineaCuenta_DTO(string nombreLinea)
        {
            this.NombreLineaCuenta = nombreLinea;
        }
    }
}
