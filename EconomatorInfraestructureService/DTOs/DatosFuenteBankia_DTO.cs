﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfraestructureService.DTOs
{
    public class DatosFuenteBankia_DTO
    {
        public Guid Id { get; set; }
        public DateTime fecha { get; set; }
        public DateTime fechaValor { get; set; }
        public string descripcion { get; set; }
        public double importe { get; set; }
        public string divisaImporte { get; set; }
        public double saldo { get; set; }
        public string divisaSaldo { get; set; }
        public string concepto1 { get; set; }
        public string concepto2 { get; set; }
        public string concepto3 { get; set; }
        public string concepto4 { get; set; }
        public string concepto5 { get; set; }
        public string concepto6 { get; set; }
        public string concepto7 { get; set; }
        public string Entidad { get; set; }
        public string LineaCuenta { get; set; }
        public string cccAsociada { get; set; }
        public string TipoFichero { get; set; }
        public DateTime DbTimeStamp { get; set; }

    }
}
