﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfraestructureService.DTOs
{
    public class Conceptos_DTO
    {
        public Guid IdConcepto { get; set; }
        public Guid presupuestoConcepto { get; set; }
        public string nombreConcepto { get; set; }
        public string descripcion { get; set; }
        public string grupoConcepto { get; set; }
        public string cuenta { get; set; }

    }
}
