﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infraestructure.Common;
using Dapper;
using InfraestructureService.DTOs;
using System.Collections;

namespace InfraestructureService
{
    public class InfraestructureServiceAccess : IDisposable
    {
        public DatosFuenteBankia_DTO dato = new DatosFuenteBankia_DTO();

        public InfraestructureServiceAccess()
        {

        }
        #region Infraestructure.Common.BdCon
        public void ExeQuery<T>(string query, IEnumerable<T> lst)
        {
            using (BdCon con = new BdCon())
            {
                con.ExeQry<T>(query, lst);
            }

        }
        public IEnumerable<T> GetDataQuery<T>(string query, IEnumerable<T> lst)
        {
            using (BdCon con = new BdCon())
            {
                return con.GetData(query, lst);
            }
        }


       
        #endregion

        #region Infraestructure.Common.FData
        public List<LineaCuenta_DTO> LineasCuentasFull()
        {
            FData fd = new FData();
            List<LineaCuenta_DTO> lst = new List<LineaCuenta_DTO>();
            foreach (var reg in fd.LineasCuentasFull())
            {
                LineaCuenta_DTO ln = new LineaCuenta_DTO();
                ln.idLineaCuenta = reg.idLineaCuenta;
                ln.NombreLineaCuenta = reg.NombreLineaCuenta;
                lst.Add(ln);
            }

            return lst;
        }
        public List<Conceptos_DTO> ConceptosFull()
        {
            FData fd = new FData();
            List<Conceptos_DTO> lst = new List<Conceptos_DTO>();
            foreach (var reg in fd.ConceptosFull())
            {
                Conceptos_DTO concept = new Conceptos_DTO();
                concept.IdConcepto = reg.IdConcepto;
                concept.nombreConcepto = reg.nombreConcepto;
                concept.descripcion = reg.descripcion;
                concept.grupoConcepto = reg.grupoConcepto;
                concept.cuenta = reg.cuenta;

                lst.Add(concept);
            }

            return lst;
        }
        public List<DatosFuenteBankia_DTO> dataBankiaFull()
        {
            FData fd = new FData();
            List<DatosFuenteBankia_DTO> lst = new List<DatosFuenteBankia_DTO>();
            foreach (var reg in fd.dataBankiaFull())
            {
                DatosFuenteBankia_DTO data = new DatosFuenteBankia_DTO();
                data.Id = reg.Id;
                data.fecha = reg.fecha;
                data.fechaValor = reg.fechaValor;
                data.descripcion = reg.descripcion;
                data.importe = reg.importe;
                data.divisaImporte = reg.divisaImporte;
                data.saldo = reg.saldo;
                data.divisaSaldo = reg.divisaSaldo;
                data.concepto1 = reg.concepto1;
                data.concepto2 = reg.concepto2;
                data.concepto3 = reg.concepto3;
                data.concepto4 = reg.concepto4;
                data.concepto5 = reg.concepto5;
                data.concepto6 = reg.concepto6;
                data.concepto7 = reg.concepto7;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.Entidad = reg.Entidad;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;

                data.cccAsociada = reg.cccAsociada;
                lst.Add(data);
            }

            return lst;
        }
        public List<gruposConceptos_DTO> gruposFull()
        {
            FData fd = new FData();
            List<gruposConceptos_DTO> lst = new List<gruposConceptos_DTO>();
            foreach (var reg in fd.GruposFull())
            {
                gruposConceptos_DTO data = new gruposConceptos_DTO();
                data.IdGrupo = reg.IdGrupo;
                data.nombreGrupo = reg.nombreGrupo;
                lst.Add(data);
            }

            return lst;
        }
        public List<Presupuestos_DTO> PresupuestosFull()
        {
            FData fd = new FData();
            List<Presupuestos_DTO> lst = new List<Presupuestos_DTO>();
            foreach (var reg in fd.PresupuestosFull())
            {
                Presupuestos_DTO presu = new Presupuestos_DTO();
                presu.IdPresupuesto = reg.IdPresupuesto;
                presu.IdTipoPresupuesto = reg.IdTipoPresupuesto;
                presu.nombrePresupuesto = reg.nombrePresupuesto;
                presu.sumaImportes = reg.sumaImportes;
                lst.Add(presu);
            }

            return lst;
        }
        public List<TiposPresupuestos_DTO> TiposPresupuestosFull()
        {
            FData fd = new FData();
            List<TiposPresupuestos_DTO> lst = new List<TiposPresupuestos_DTO>();
            foreach (var reg in fd.TipoPresupuestosFull())
            {
                TiposPresupuestos_DTO Tipopresu = new TiposPresupuestos_DTO();
                Tipopresu.IdTipoPresupuesto = reg.IdTipoPresupuesto;
                Tipopresu.nombreTipo = reg.nombreTipo;
                Tipopresu.importe1 = reg.importe1;
                Tipopresu.importe2 = reg.importe2;
                Tipopresu.importe3 = reg.importe3;
                Tipopresu.importe4 = reg.importe4;
                Tipopresu.importe5 = reg.importe5;
                Tipopresu.importe6 = reg.importe6;
                Tipopresu.importe7 = reg.importe7;
                Tipopresu.importe8 = reg.importe8;
                Tipopresu.importe9 = reg.importe9;
                Tipopresu.importe10 = reg.importe10;
                Tipopresu.importe11 = reg.importe11;
                Tipopresu.importe12 = reg.importe12;
                Tipopresu.importe13 = reg.importe13;
                Tipopresu.importe14 = reg.importe14;
                Tipopresu.importe15 = reg.importe15;
                Tipopresu.importe16 = reg.importe16;
                Tipopresu.importe17 = reg.importe17;
                Tipopresu.importe18 = reg.importe18;
                Tipopresu.importe19 = reg.importe19;
                Tipopresu.importe20 = reg.importe20;
                Tipopresu.importe21 = reg.importe21;
                Tipopresu.importe22 = reg.importe22;
                Tipopresu.importe23 = reg.importe23;
                Tipopresu.importe24 = reg.importe24;
                Tipopresu.importe25 = reg.importe25;
                Tipopresu.importe26 = reg.importe26;
                Tipopresu.importe26 = reg.importe27;
                Tipopresu.importe28 = reg.importe28;
                Tipopresu.importe29 = reg.importe29;
                Tipopresu.importe30 = reg.importe30;
                Tipopresu.importe31 = reg.importe31;

                lst.Add(Tipopresu);
            }

            return lst;
        }
        public List<cuentas_DTO> CuentasFull()
        {
            FData fd = new FData();
            List<cuentas_DTO> lst = new List<cuentas_DTO>();
            foreach (var reg in fd.cuentasFull())
            {
                cuentas_DTO ccc = new cuentas_DTO();
                ccc.idCuenta = reg.idCuenta;
                ccc.nombreCCC = reg.nombreCCC;
                ccc.numeroCCC = reg.numeroCCC;
                ccc.entidadCCC = reg.entidadCCC;
                ccc.operativa = reg.operativa;
                ccc.LineaCuenta = reg.LineaCuenta;
                ccc.fecha_alta = reg.fecha_alta;
                ccc.fecha_baja = reg.fecha_baja;
                ccc.comentarios = reg.comentarios;



                lst.Add(ccc);
            }

            return lst;
        }
        #endregion

        public void Dispose()
        {

        }

    }
}
