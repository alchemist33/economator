﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace InfraestructureService.DTOsTy2
{
    public class ChartParams_DTO
    {
        public string SerieName { get; set; }
        public string TypeChart { get; set; }
        public bool presupuesto { get; set; }
        public string grpConcepts { get; set; }
        public int year { get; set; }
        public Chart Grafica { get; set; }
        public Series Serie { get; set; }
        public Series SerieP { get; set; }

        public ChartParams_DTO(Series sr = null)
        {
            if (sr == null)
            {
                this.Serie = new Series();
                this.SerieP = new Series();
            }
            else
            {
                this.Serie = sr;
                this.SerieP = sr;
            }

        }
    }
}
