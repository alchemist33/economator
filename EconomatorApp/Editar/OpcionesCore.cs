﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InfraestructureService;
using InfraestructureService.DTOs;

namespace EconomatorApp.Editar
{
    public class OpcionesCore
    {
        public enum dbSelector
        {
            SqlServer,
            SqlCompact,
            Sqlite

        }

        public void selectDbProvider(dbSelector selection)
        {
            switch (selection)
            {
                case dbSelector.SqlServer:

                    break;

                case dbSelector.SqlCompact:

                    break;

                case dbSelector.Sqlite:
                    break;
            }
        }

        public List<string> getOptionValues(string optionName)
        {
            string values = "";
            string query = $"SELECT optionValue FROM Opciones WHERE optionName = '{optionName}' ";

            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                values = isa.GetDataQuery<string>(query, null).FirstOrDefault();
            }
            return values.Split(';').ToList();
        }

        public void updateValorOpcion(Opciones_DTO option)
        {
            string query = $"UPDATE Opciones SET optionValue = '{option.optionValue}' WHERE idOption = '{option.idOption}'";

            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                isa.ExeQuery<Opciones_DTO>(query, null);

            }
        }

    }
}
