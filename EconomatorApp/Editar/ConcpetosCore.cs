﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InfraestructureService.DTOs;
using InfraestructureService;

namespace EconomatorApp.Editar
{
    public class ConcpetosCore
    {
        errorControl errCn = new errorControl();
        public void updateConceptos()
        {
          
            List<DatosFuenteBankia_DTO> conceptosOrigenBankia = new List<DatosFuenteBankia_DTO>();
            List<DatosFuenteING_DTO> conceptosOrigenING = new List<DatosFuenteING_DTO>();
            List<Conceptos_DTO> conceptosTabla = new List<Conceptos_DTO>();
   
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                string query = "SELECT DISTINCT descripcion,cccAsociada FROM datosOrigenBankia ORDER BY descripcion ASC ";
                string query2 = "SELECT DISTINCT descripcion,cccAsociada FROM datosOrigenING ORDER BY descripcion ASC ";
                string query3 = "SELECT DISTINCT nombreConcepto,cuenta FROM Conceptos ORDER BY nombreConcepto ASC";
                conceptosOrigenBankia = isa.GetDataQuery<DatosFuenteBankia_DTO>(query, null).ToList();
                conceptosOrigenING = isa.GetDataQuery<DatosFuenteING_DTO>(query2, null).ToList();
                conceptosTabla = isa.GetDataQuery<Conceptos_DTO>(query3, null).ToList();
            }
            List<DatosFuenteBankia_DTO> lstBankia = new List<DatosFuenteBankia_DTO>(conceptosOrigenBankia);
            List<DatosFuenteING_DTO> lstING = new List<DatosFuenteING_DTO>(conceptosOrigenING);
            if (conceptosTabla.Count == 0)
            {
                foreach (var reg in conceptosOrigenBankia)
                {
                    string query = $@"INSERT INTO Conceptos(IdConcepto,nombreConcepto,cuenta) VALUES(NEWID(),'{reg.descripcion}','{reg.cccAsociada}')";
                    using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                    {
                        isa.ExeQuery<Conceptos_DTO>(query, null);
                    }
                }

                foreach (var reg in conceptosOrigenING)
                {
                    string query = $@"INSERT INTO Conceptos(IdConcepto,nombreConcepto,cuenta) VALUES(NEWID(),'{reg.descripcion}','{reg.cccAsociada}')";
                    using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                    {
                        isa.ExeQuery<Conceptos_DTO>(query, null);
                    }
                }
            }
            else
            {
               
                foreach (var reg in conceptosOrigenBankia)
                {
                    foreach(var reg2 in conceptosTabla)
                    {
                        if(reg.descripcion == reg2.nombreConcepto && reg.cccAsociada == reg2.cuenta)
                        {
                            lstBankia.Remove(reg);
                        }
                    }
                }
                foreach (var reg in conceptosOrigenING)
                {
                    foreach (var reg2 in conceptosTabla)
                    {
                        if (reg.descripcion == reg2.nombreConcepto && reg.cccAsociada == reg2.cuenta)
                        {
                            lstING.Remove(reg);
                        }
                    }
                }

                if (lstBankia != null && lstBankia.Count > 0)
                {
                    foreach (var reg in lstBankia)
                    {
                        string query = $@"INSERT INTO Conceptos(IdConcepto,nombreConcepto,presupuestoCocepto,cuenta) VALUES(NEWID(),'{reg.descripcion}','00000000-0000-0000-0000-000000000000','{reg.cccAsociada}')";
                        using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                        {
                            isa.ExeQuery<Conceptos_DTO>(query, null);
                        }
                    }
                }
                if (lstING != null && lstING.Count > 0)
                {
                    foreach (var reg in lstING)
                    {
                        string query = $@"INSERT INTO Conceptos(IdConcepto,nombreConcepto,presupuestoCocepto,cuenta) VALUES(NEWID(),'{reg.descripcion}','00000000-0000-0000-0000-000000000000','{reg.cccAsociada}')";
                        using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                        {
                            isa.ExeQuery<Conceptos_DTO>(query, null);
                        }
                    }
                }
            }
            MessageBox.Show($"{lstBankia.Count + lstING.Count} conceptos actualizados");
        }
        public void nuevoGrupo(string nombreGrupo)
        {
            try
            {
                string query = $"INSERT INTO gruposConceptos(IdGrupo,nombreGrupo) VALUES(NEWID(),'{nombreGrupo}')";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<gruposConceptos_DTO>(query, null);
                }
                MessageBox.Show($"Grupo {nombreGrupo}, Creado con exito!");
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }


        }
        public void deleteGrupo(string nombreGrupo)
        {
            try
            {
                string query = $"DELETE FROM gruposConceptos WHERE nombreGrupo LIKE '{nombreGrupo}'";
                string query2 = $"UPDATE Conceptos SET grupoConcepto = NULL WHERE grupoConcepto LIKE '{nombreGrupo}' ";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<gruposConceptos_DTO>(query, null);
                    isa.ExeQuery<gruposConceptos_DTO>(query2, null);
                }
                MessageBox.Show($@"El grupo: {nombreGrupo}. Ha sido eliminado con exito!
                                Los conceptos aigandos ya no tienen este grupo vinvulado", "Grupo borrado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }

        }
        public void asignarGrupo(string concepto, string grupo)
        {
            try
            {
                string query = $"UPDATE Conceptos SET grupoConcepto = '{grupo}' WHERE nombreConcepto LIKE '{concepto}'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<Conceptos_DTO>(query, null);
                }
                MessageBox.Show($"El concepto -{concepto}- ha sido AGREGADO al grupo de -{grupo}-");
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex, "Metodo: asignarGrupo()");
            }
        }
        public void quitarGrupo(string concepto, string grupo)
        {
            try
            {
                string query = $"UPDATE Conceptos SET grupoConcepto = '{grupo}' WHERE nombreConcepto = '{concepto}' ";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<Conceptos_DTO>(query, null);
                }
                MessageBox.Show($"El concepto -{concepto}- ha sido ELIMINADO del grupo de -{grupo}-");
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
        }
        public bool updateGrupo(string grupoNew, string grupoOld)
        {
            try
            {
                string query = $"UPDATE gruposConceptos SET nombreGrupo = '{grupoNew}' WHERE nombreGrupo LIKE '{grupoOld}'";
                string query2 = $"UPDATE Conceptos SET grupoConcepto = '{grupoNew}' WHERE grupoConcepto LIKE '{grupoOld}'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<string>(query, null);
                    isa.ExeQuery<Conceptos_DTO>(query2, null);
                    return true;
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
                return false;
            }

        }

        /// <summary>
        /// Este metodo obtiene todos los grupos a los que pertenece un Concepto
        /// </summary>
        /// <param name="concepto"></param>
        /// <returns></returns>
        public List<Conceptos_DTO> getGruposConcepto(string concepto)
        {
            List<Conceptos_DTO> ret = new List<Conceptos_DTO>();
            try
            {
                string query = $"SELECT * FROM Conceptos WHERE nombreConcepto LIKE '{concepto}'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    ret = isa.GetDataQuery<Conceptos_DTO>(query, null).ToList(); ;
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
            return ret;
        }

        /// <summary>
        /// Este metodo obtiene todos los conceptos asigandos a un grupo
        ///      /// </summary>
        /// <param name="grupo"></param>
        /// <returns></returns>
        public List<Conceptos_DTO> getConceptosGrupo(string grupo)
        {
            List<Conceptos_DTO> ret = new List<Conceptos_DTO>();
            try
            {
                string query = $"SELECT * FROM Conceptos WHERE grupoConcepto LIKE '%{grupo}%'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    ret = isa.GetDataQuery<Conceptos_DTO>(query, null).ToList(); ;
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
            return ret;
        }


        public List<DatosFuenteBankia_DTO> getDetalleConceptoBankia(string concepto)
        {
            string query = $"SELECT * FROM DatosOrigenBankia WHERE descripcion LIKE '{concepto}'";
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
               return isa.GetDataQuery<DatosFuenteBankia_DTO>(query, null).ToList();
            }
        }
        public List<DatosFuenteING_DTO> getDetalleConceptoING(string concepto)
        {
            string query = $"SELECT * FROM DatosOrigenING WHERE descripcion LIKE '{concepto}'";
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                return isa.GetDataQuery<DatosFuenteING_DTO>(query, null).ToList();
            }
        }

        public List<string> BuscaConceptos(string busqueda)
        {
            string query = $"SELECT nombreConcepto FROM Conceptos WHERE nombreConcepto LIKE '%{busqueda}%'";
            List<string> lst = new List<string>();
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                lst = isa.GetDataQuery<string>(query, null).ToList();
            }
            return lst;
        }

        public string getPresupuestoConcepto(string concept)
        {
            Guid ?idPresu = new Guid();
            string nomPresu = String.Empty;
           
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                string query = $"SELECT presupuestoConcepto FROM Conceptos WHERE nombreConcepto = '{concept.Split('|').FirstOrDefault()}' AND cuenta = '{concept.Split('|').LastOrDefault()}'";
               
                idPresu = isa.GetDataQuery<Guid>(query, null).FirstOrDefault();
                string query2 = $"SELECT nombrePresupuesto FROM Presupuestos WHERE idPresupuesto = '{idPresu}'";

                nomPresu = isa.GetDataQuery<string>(query2, null).FirstOrDefault();
                if(!String.IsNullOrEmpty(nomPresu))
                {
                    return nomPresu;
                }
                else
                {
                    return "Ninguno";
                }
            } 
        }

        public string getCuentaConcepto(string concpet)
        {
            string query = $"SELECT cuenta FROM Conceptos WHERE nombreConcpeto = '{concpet}'";
            using(InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
               return isa.GetDataQuery<string>(query, null).FirstOrDefault();
            }
        }
    }
}
