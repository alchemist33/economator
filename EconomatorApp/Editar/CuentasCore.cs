﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InfraestructureService.DTOs;
using InfraestructureService;
   


namespace EconomatorApp.Editar
{
    public class CuentasCore
    {
        errorControl errCn = new errorControl();

        

        public void nuevaCuenta(cuentas_DTO ccc)
        {
            try
            {
                if (ccc.nombreCCC.Equals(string.Empty) || ccc.entidadCCC.Equals(string.Empty) || ccc.numeroCCC.Equals(string.Empty))
                {
                    MessageBox.Show("Revisar campos vacios","error agregando cuenta",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else
                {

                    string query = $@"INSERT INTO CCCs(idCuenta,LineaCuenta,numeroCCC,entidadCCC,nombreCCC,operativa,fecha_alta,fecha_baja,comentarios)
                                          VALUES (NEWID(),'{ccc.LineaCuenta}','{ccc.numeroCCC}','{ccc.entidadCCC}','{ccc.nombreCCC}','{ccc.operativa}','{ccc.fecha_alta}','{ccc.fecha_baja}','{ccc.comentarios}')";
                    using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                    {
                        isa.ExeQuery<cuentas_DTO>(query, null);
                    }
                    MessageBox.Show("Cuenta agregada correctamente");
                   
                }

            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
        }

        public void deleteCuenta(Guid Idccc)
        {
            try
            {

                    string query = $@"DELETE FROM CCCs WHERE idCuenta = '{Idccc}'";
                    using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                    {
                        isa.ExeQuery<cuentas_DTO>(query, null);
                    }
                    MessageBox.Show("Cuenta eliminada correctamente");
                

            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
        }
        public void updateCuenta(cuentas_DTO ccc, string cuenta_old)
        {
            try
            {
                string query2 = $"UPDATE datosOrigenBankia SET cccAsociada = '{ccc.nombreCCC}',LineaCuenta = '{ccc.LineaCuenta}',Entidad = '{ccc.entidadCCC}' WHERE cccAsociada = (SELECT nombreCCC FROM CCCs WHERE idCuenta = '{ccc.idCuenta}')";
                string query3 = $"UPDATE datosOrigenBankiaHist SET cccAsociada = '{ccc.nombreCCC}',LineaCuenta = '{ccc.LineaCuenta}',Entidad = '{ccc.entidadCCC}' WHERE cccAsociada  = (SELECT nombreCCC FROM CCCs WHERE idCuenta = '{ccc.idCuenta}')";
                string query4 = $"UPDATE datosOrigenING SET cccAsociada = '{ccc.nombreCCC}',LineaCuenta = '{ccc.LineaCuenta}',Entidad = '{ccc.entidadCCC}' WHERE cccAsociada = (SELECT nombreCCC FROM CCCs WHERE idCuenta = '{ccc.idCuenta}')";
                string query5 = $"UPDATE datosOrigenINGHist SET cccAsociada = '{ccc.nombreCCC}',LineaCuenta = '{ccc.LineaCuenta}',Entidad = '{ccc.entidadCCC}' WHERE cccAsociada  = (SELECT nombreCCC FROM CCCs WHERE idCuenta = '{ccc.idCuenta}')";

                string query = @$"UPDATE CCCs SET LineaCuenta = '{ccc.LineaCuenta}', 
                                              operativa = '{ccc.operativa}', 
                                              nombreCCC = '{ccc.nombreCCC}', 
                                              entidadCCC = '{ccc.entidadCCC}', 
                                              numeroCCC = '{ccc.numeroCCC}',  
                                              fecha_alta = '{ccc.fecha_alta}', 
                                              fecha_baja = '{ccc.fecha_baja}', 
                                              comentarios = '{ccc.comentarios}' 
                                WHERE idCuenta = '{ccc.idCuenta}'";

                string query6 = @$"UPDATE Conceptos SET cuenta = '{ccc.nombreCCC}' WHERE cuenta = '{cuenta_old}'";



                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<DatosFuenteBankia_DTO>(query2, null);
                    isa.ExeQuery<DatosFuenteBankia_DTO>(query3, null);
                    isa.ExeQuery<DatosFuenteING_DTO>(query4, null);
                    isa.ExeQuery<DatosFuenteING_DTO>(query5, null);
                    isa.ExeQuery<Conceptos_DTO>(query6, null);
                    isa.ExeQuery<cuentas_DTO>(query, null);
                }
                MessageBox.Show("Cuenta actualizada correctamente");
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
           
        }

    }

    
}
