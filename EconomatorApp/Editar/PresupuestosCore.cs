﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfraestructureService;
using InfraestructureService.DTOs;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EconomatorApp.Editar
{
    public class PresupuestosCore
    {
        errorControl errCn = new errorControl();
        public void nuevoTipoPresupuesto(string nomTipoPresu)
        {
            try
            {
                if (nomTipoPresu != "")
                {
                    string query = $@"INSERT INTO TiposPresupuestos (IdTipoPresupuesto,nombreTipo) VALUES(NEWID(),'{nomTipoPresu}')";
                    using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                    {
                        isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                    }
                    MessageBox.Show($"El Tipo de presupuesto {nomTipoPresu}, se ha agregado correctamente!", "Tipo de presupuesto agreado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Exception ex = new Exception();
                    errCn.errorSelector(errorControl.CustomDialog.Error, ex);
                }
            }
            catch (SqlException sqlEx)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, sqlEx, "Has introducido un nombre para el tipo de presupuesto?");
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }
        }

        public void updateTipoPresupuesto(string new_nomTipoPresu, string old_nomTipoPresu)
        {
            try
            {
                if (new_nomTipoPresu != "")
                {
                    string query = $@"Update TiposPresupuestos set nombreTipo = '{new_nomTipoPresu}' WHERE idTipoPresupuesto = '{getIdTipoPresu(old_nomTipoPresu).IdTipoPresupuesto}'";
                    using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                    {
                        isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                    }
                    MessageBox.Show($"El Tipo de presupuesto {old_nomTipoPresu}, se ha actualizado correctamente!", "Tipo de presupuesto agreado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Exception ex = new Exception();
                    errCn.errorSelector(errorControl.CustomDialog.Error, ex);
                }
            }
            catch (SqlException sqlEx)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, sqlEx, "Has introducido un nombre para el tipo de presupuesto?");
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }
        }
        public void nuevoPresupuesto(string nomPresu, string tipoPresu, Guid idTipoPresu, bool sumaImportes)
        {
            try
            {
                string query = $@"INSERT INTO Presupuestos (IdPresupuesto,nombrePresupuesto,IdTipoPresupuesto,sumaImportes) VALUES(NEWID(),'{nomPresu}','{idTipoPresu}','{sumaImportes}')";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                }
                MessageBox.Show($"El Presupuesto: {nomPresu} (con el tipo: {tipoPresu}), se ha agregado correctamente! ", "Presupuesto agreado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }
        }

        public void updatePresupuesto(string new_nomPresu, string old_nomPresu, string tipoPresu, bool sumaImportes)
        {
            try
            {
                string query = $@"Update Presupuestos set nombrePresupuesto = '{new_nomPresu}', idTipoPresupuesto = '{getIdTipoPresu(tipoPresu).IdTipoPresupuesto}', sumaImportes = '{sumaImportes}' WHERE idPresupuesto = '{getIdPresu(old_nomPresu).IdPresupuesto}'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<Presupuestos_DTO>(query, null);
                }
                MessageBox.Show($"El Presupuesto: {old_nomPresu}, se ha actualizado correctamente! ", "Presupuesto actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }
        }
        public TiposPresupuestos_DTO getIdTipoPresu(string nomTipoPresu)
        {
            TiposPresupuestos_DTO result = null;
            try
            {
                string query = $"SELECT idTipoPresupuesto FROM TiposPresupuestos WHERE nombreTipo LIKE '{nomTipoPresu}'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    result = isa.GetDataQuery<TiposPresupuestos_DTO>(query, null).FirstOrDefault(); ;
                }
            }
            catch (Exception ex)
            {

                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }

            return result;
        }
        public Presupuestos_DTO getIdPresu(string nomPresu)
        {
            Presupuestos_DTO result = null;
            try
            {
                string query = $"SELECT idPresupuesto FROM Presupuestos WHERE nombrePresupuesto LIKE '{nomPresu}'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    result = isa.GetDataQuery<Presupuestos_DTO>(query, null).FirstOrDefault(); ;
                }
            }
            catch (Exception ex)
            {

                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }

            return result;
        }
        public void deleteTipoPresupuesto(string nomTipoPresu)
        {
            try
            {
                string query = $"DELETE FROM TiposPresupuestos WHERE idTipoPresupuesto = '{getIdTipoPresu(nomTipoPresu).IdTipoPresupuesto}' ";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                }
                MessageBox.Show($@"El tipo de presupuesto: {nomTipoPresu}. Ha sido eliminado con exito!
                                Los presupuestos que tenian es tipo asignado se han asignado al tipo default", "Tipo de presupuesto borrado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }

        }
        public void deletePresupuesto(string nomPresu)
        {
            try
            {
                string query = $"DELETE FROM Presupuestos WHERE nombrePresupuesto = '{nomPresu}' ";
                string query2 = $"UPDATE Conceptos set presupuestoConcepto = '00000000-0000-0000-0000-000000000000' WHERE presupuestoConcepto = '{getIdPresu(nomPresu).IdPresupuesto}'";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                    isa.ExeQuery<TiposPresupuestos_DTO>(query2, null);
                }
                MessageBox.Show($@"El presupuesto: {nomPresu}. Ha sido eliminado con exito!", "Tipo de presupuesto borrado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.ErrorLv2, ex);
            }

        }

        public void nuevoImporte(string nomTipoPresu, double CantidadImporte)
        {
            
            TiposPresupuestos_DTO reg = new TiposPresupuestos_DTO();
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                string query = $"SELECT * FROM TiposPresupuestos WHERE idTipoPresupuesto = '{getIdTipoPresu(nomTipoPresu).IdTipoPresupuesto}'";

                reg = isa.GetDataQuery<TiposPresupuestos_DTO>(query, null).First();
            }
            string importeN = "";
            importeN = reg.importe1 > 0 ? "" : "importe1";
            if (String.IsNullOrEmpty(importeN))
            {
                importeN = reg.importe2 > 0 ? "" : "importe2";
                if (String.IsNullOrEmpty(importeN))
                {
                    importeN = reg.importe3 > 0 ? "" : "importe3";
                    if (String.IsNullOrEmpty(importeN))
                    {
                        importeN = reg.importe4 > 0 ? "" : "importe4";
                        if (String.IsNullOrEmpty(importeN))
                        {
                            importeN = reg.importe5 > 0 ? "" : "importe5";
                            if (String.IsNullOrEmpty(importeN))
                            {
                                importeN = reg.importe6 > 0 ? "" : "importe6";
                                if (String.IsNullOrEmpty(importeN))
                                {
                                    importeN = reg.importe7 > 0 ? "" : "importe7";
                                    if (String.IsNullOrEmpty(importeN))
                                    {
                                        importeN = reg.importe8 > 0 ? "" : "importe8";
                                        if (String.IsNullOrEmpty(importeN))
                                        {
                                            importeN = reg.importe9 > 0 ? "" : "importe9";
                                            if (String.IsNullOrEmpty(importeN))
                                            {
                                                importeN = reg.importe10 > 0 ? "" : "importe10";
                                                if (String.IsNullOrEmpty(importeN))
                                                {
                                                    importeN = reg.importe11 > 0 ? "" : "importe11";
                                                    if (String.IsNullOrEmpty(importeN))
                                                    {
                                                        importeN = reg.importe12 > 0 ? "" : "importe12";
                                                        if (String.IsNullOrEmpty(importeN))
                                                        {
                                                            importeN = reg.importe13 > 0 ? "" : "importe13";
                                                            if (String.IsNullOrEmpty(importeN))
                                                            {
                                                                importeN = reg.importe14 > 0 ? "" : "importe14";
                                                                if (String.IsNullOrEmpty(importeN))
                                                                {
                                                                    importeN = reg.importe15 > 0 ? "" : "importe15";
                                                                    if (String.IsNullOrEmpty(importeN))
                                                                    {
                                                                        importeN = reg.importe16 > 0 ? "" : "importe16";
                                                                        if (String.IsNullOrEmpty(importeN))
                                                                        {
                                                                            importeN = reg.importe17 > 0 ? "" : "importe17";
                                                                            if (String.IsNullOrEmpty(importeN))
                                                                            {
                                                                                importeN = reg.importe18 > 0 ? "" : "importe18";
                                                                                if (String.IsNullOrEmpty(importeN))
                                                                                {
                                                                                    importeN = reg.importe19 > 0 ? "" : "importe19";
                                                                                    if (String.IsNullOrEmpty(importeN))
                                                                                    {
                                                                                        importeN = reg.importe20 > 0 ? "" : "importe20";
                                                                                        if (String.IsNullOrEmpty(importeN))
                                                                                        {
                                                                                            importeN = reg.importe21 > 0 ? "" : "importe21";
                                                                                            if (String.IsNullOrEmpty(importeN))
                                                                                            {
                                                                                                importeN = reg.importe22 > 0 ? "" : "importe22";
                                                                                                if (String.IsNullOrEmpty(importeN))
                                                                                                {
                                                                                                    importeN = reg.importe23 > 0 ? "" : "importe23";
                                                                                                    if (String.IsNullOrEmpty(importeN))
                                                                                                    {
                                                                                                        importeN = reg.importe24 > 0 ? "" : "importe24";
                                                                                                        if (String.IsNullOrEmpty(importeN))
                                                                                                        {
                                                                                                            importeN = reg.importe25 > 0 ? "" : "importe25";
                                                                                                            if (String.IsNullOrEmpty(importeN))
                                                                                                            {
                                                                                                                importeN = reg.importe26 > 0 ? "" : "importe26";
                                                                                                                if (String.IsNullOrEmpty(importeN))
                                                                                                                {
                                                                                                                    importeN = reg.importe27 > 0 ? "" : "importe27";
                                                                                                                    if (String.IsNullOrEmpty(importeN))
                                                                                                                    {
                                                                                                                        importeN = reg.importe28 > 0 ? "" : "importe28";
                                                                                                                        if (String.IsNullOrEmpty(importeN))
                                                                                                                        {
                                                                                                                            importeN = reg.importe29 > 0 ? "" : "importe29";
                                                                                                                            if (String.IsNullOrEmpty(importeN))
                                                                                                                            {
                                                                                                                                importeN = reg.importe30 > 0 ? "" : "importe30";
                                                                                                                                if (String.IsNullOrEmpty(importeN))
                                                                                                                                {
                                                                                                                                    importeN = reg.importe31 > 0 ? "" : "importe31";
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                string query = $@"Update TiposPresupuestos set {importeN} = '{CantidadImporte}' WHERE idTipoPresupuesto = '{getIdTipoPresu(nomTipoPresu).IdTipoPresupuesto}'";
                isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                MessageBox.Show($@"El Importe: {importeN}. Ha sido agregado con el valor de: {CantidadImporte}", "Importe agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        public void updateImporte(string nomTipoPresu, double CantidadImporte, string importeN)
        {

            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                string query = $@"Update TiposPresupuestos set {importeN} = '{CantidadImporte}' WHERE idTipoPresupuesto = '{getIdTipoPresu(nomTipoPresu).IdTipoPresupuesto}'";
                isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                MessageBox.Show($@"El Importe: {importeN}. Ha sido actualizado con el valor de: {CantidadImporte}", "Importe actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        public void deleteImporte(string nomTipoPresu, string importeN)
        {

            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                string query = $@"Update TiposPresupuestos set {importeN} = 0 WHERE idTipoPresupuesto = '{getIdTipoPresu(nomTipoPresu).IdTipoPresupuesto}'";
                isa.ExeQuery<TiposPresupuestos_DTO>(query, null);
                MessageBox.Show($@"El Importe: {importeN}. Ha sido borrado ", "Importe borrado", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        public void asignarPresupuesto(string concepto, string nomPresu)
        {
            ConcpetosCore cc = new ConcpetosCore();
           
            string query = $"UPDATE Conceptos Set presupuestoConcepto = '{getIdPresu(nomPresu).IdPresupuesto}' WHERE nombreConcepto = '{concepto.Split('|').FirstOrDefault()}' AND cuenta = '{concepto.Split('|').LastOrDefault()}' ";
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                isa.ExeQuery<Conceptos_DTO>(query, null);
            }
            MessageBox.Show($"El presupuesto: {nomPresu} se ha asignado al concpeto: {concepto.Split('|').FirstOrDefault()} con exito", "Presupuesto asignado",MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
