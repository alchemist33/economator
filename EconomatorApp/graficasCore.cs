﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using EconomatorApp.Editar;
using InfraestructureService.DTOs;
using InfraestructureService.DTOsTy2;

namespace EconomatorApp
{
    public class graficasCore
    {
        ConcpetosCore edConCore = new ConcpetosCore();
        operaciones op = new operaciones();
        #region Utils
        public string nomMes(int numMes)
        {
            string nom = string.Empty;
            switch (numMes)
            {
                case 1:
                    nom = "Enero";
                    break;
                case 2:
                    nom = "Febrero";
                    break;
                case 3:
                    nom = "Marzo";
                    break;
                case 4:
                    nom = "Abril";
                    break;
                case 5:
                    nom = "Mayo";
                    break;
                case 6:
                    nom = "Junio";
                    break;
                case 7:
                    nom = "Julio";
                    break;
                case 8:
                    nom = "Agosto";
                    break;
                case 9:
                    nom = "Septiembre";
                    break;
                case 10:
                    nom = "Octubre";
                    break;
                case 11:
                    nom = "Noviembre";
                    break;
                case 12:
                    nom = "Diciembre";
                    break;
            }
            return nom;
        }
        public List<DateTime> getInicioFinYear(int year)
        {
            DateTime inicio = new DateTime(year, 01, 01);
            DateTime fin = new DateTime(year, 12, 31);
            List<DateTime> fechas = new List<DateTime>();
            fechas.Add(inicio);
            fechas.Add(fin);

            return fechas;
        }
        #endregion

        public void datosGrupos1Yr(ChartParams_DTO chP)
        {
            operaciones opAux = new operaciones("Z1");
            List<Conceptos_DTO> lstCon = new List<Conceptos_DTO>(edConCore.getConceptosGrupo(chP.grpConcepts));
            List<DatosFuenteBankia_DTO> lstData = new List<DatosFuenteBankia_DTO>( // lista segun fecha (tiempo transcurrido de año actual o cualquier año pasado completo)
                opAux.lstDataBankia.Where<DatosFuenteBankia_DTO>(x => x.fecha >= getInicioFinYear(chP.year)[0] && x.fecha <= getInicioFinYear(chP.year)[1]).ToList());
            List<DatosFuenteBankia_DTO> LstDataAux = new List<DatosFuenteBankia_DTO>();
            List<TiposPresupuestos_DTO> lstTiposPresu = new List<TiposPresupuestos_DTO>();
            List<Presupuestos_DTO> lstPresu = new List<Presupuestos_DTO>();



            foreach (var reg in lstCon)
            {
                foreach (var reg2 in lstData)
                {
                    if (reg2.descripcion == reg.nombreConcepto)
                    {
                        LstDataAux.Add(reg2); // lista segun fecha y conceptos de un grupo
                    }
                }
            }

            for (int i = 1; i <= 12; i++)
            {
                var lstDataF = LstDataAux.Distinct().Where<DatosFuenteBankia_DTO>(x => x.fecha.Month == i).ToList();
                double totalMes = lstDataF.Sum<DatosFuenteBankia_DTO>(x => x.importe) * -1;

                //sr.Points.AddXY(nomMes(i), totalMes);
                chP.Serie.Points.Add(getCustomPoints(nomMes(i), totalMes));
            }
            
            if (chP.presupuesto)
            {
                lstTiposPresu = opAux.lstTyPresu;
                lstPresu = opAux.lstPresu;
                List<Guid> presusConceptos = lstCon.Select(x => x.presupuestoConcepto).ToList();
                double presuEnero = 0;
                double presuFebrero = 0;
                double presuMarzo = 0;
                double presuAbril = 0;
                double presuMayo = 0;
                double presuJunio = 0;
                double presuJulio = 0;
                double presuAgosto = 0;
                double presuSeptiembre = 0;
                double presuOctubre = 0;
                double presuNoviembre = 0;
                double presuDiciembre = 0;
                double aux_presuEnero = 0;
                double aux_presuFebrero = 0;
                double aux_presuMarzo = 0;
                double aux_presuAbril = 0;
                double aux_presuMayo = 0;
                double aux_presuJunio = 0;
                double aux_presuJulio = 0;
                double aux_presuAgosto = 0;
                double aux_presuSeptiembre = 0;
                double aux_presuOctubre = 0;
                double aux_presuNoviembre = 0;
                double aux_presuDiciembre = 0;
                Guid idTypePresu = new Guid();
                foreach (var reg in lstCon)
                {
                    foreach (var reg2 in lstPresu)
                    {
                        if (reg.presupuestoConcepto == reg2.IdPresupuesto)
                        {
                            idTypePresu = reg2.IdTipoPresupuesto;
                            if (reg2.sumaImportes)
                            {
                                presuEnero += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe1).FirstOrDefault();
                                presuFebrero += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe2).FirstOrDefault();
                                presuMarzo += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe3).FirstOrDefault();
                                presuAbril += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe4).FirstOrDefault();
                                presuMayo += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe5).FirstOrDefault();
                                presuJunio += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe6).FirstOrDefault();
                                presuJulio += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe7).FirstOrDefault();
                                presuAgosto += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe8).FirstOrDefault();
                                presuSeptiembre += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe9).FirstOrDefault();
                                presuOctubre += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe10).FirstOrDefault();
                                presuNoviembre += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe11).FirstOrDefault();
                                presuDiciembre += lstTiposPresu.Where(x => x.IdTipoPresupuesto == idTypePresu).Select(x => x.importe12).FirstOrDefault();
                            }
                        }
                    }
                }
                List<Guid> lstIds = new List<Guid>();
                foreach (var reg in lstCon)
                {
                    foreach (var reg2 in lstPresu)
                    {
                        if (reg.presupuestoConcepto == reg2.IdPresupuesto)
                        {
                            if (!reg2.sumaImportes)
                            {
                                lstIds.Add(reg2.IdTipoPresupuesto);
                            }
                        }
                    }
                }
                var lst_aux = lstIds.Distinct();
                foreach (var reg3 in lst_aux)
                {
                    aux_presuEnero = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe1).FirstOrDefault();
                    aux_presuFebrero = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe2).FirstOrDefault();
                    aux_presuMarzo = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe3).FirstOrDefault();
                    aux_presuAbril = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe4).FirstOrDefault();
                    aux_presuMayo = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe5).FirstOrDefault();
                    aux_presuJunio = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe6).FirstOrDefault();
                    aux_presuJulio = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe7).FirstOrDefault();
                    aux_presuAgosto = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe8).FirstOrDefault();
                    aux_presuSeptiembre = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe9).FirstOrDefault();
                    aux_presuOctubre = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe10).FirstOrDefault();
                    aux_presuNoviembre = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe11).FirstOrDefault();
                    aux_presuDiciembre = lstTiposPresu.Where(x => x.IdTipoPresupuesto == reg3).Select(x => x.importe12).FirstOrDefault();

                    presuEnero += aux_presuEnero;
                    presuFebrero += aux_presuFebrero;
                    presuMarzo += aux_presuMarzo;
                    presuAbril += aux_presuAbril;
                    presuMayo += aux_presuMayo;
                    presuJunio += aux_presuJunio;
                    presuJulio += aux_presuJulio;
                    presuAgosto += aux_presuAgosto;
                    presuSeptiembre += aux_presuSeptiembre;
                    presuOctubre += aux_presuOctubre;
                    presuNoviembre += aux_presuNoviembre;
                    presuDiciembre += aux_presuDiciembre;
                }

                chP.SerieP.Points.Add(getCustomPoints(nomMes(1), presuEnero));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(2), presuFebrero));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(3), presuMarzo));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(4), presuAbril));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(5), presuMayo));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(6), presuJunio));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(7), presuJulio));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(8), presuAgosto));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(9), presuSeptiembre));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(10), presuOctubre));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(11), presuNoviembre));
                chP.SerieP.Points.Add(getCustomPoints(nomMes(12), presuDiciembre));

            }

            generaGrafica(chP);
        }

        #region Custom Charts
        private void generaGrafica(ChartParams_DTO chP)
        {

            chP.Grafica.Series.Clear();
            chP.Grafica.ChartAreas.Clear();


            chP.Grafica.ChartAreas.Add(getCustomChartArea(ChartAreaType.Type1));
            chP.Grafica.Series.Add(getCustomSeries(tiposGraficas.ColumnaT1, chP));
            chP.Grafica.Series.Add(getCustomSeriesP(tiposGraficas.ColumnaT1, chP));
        }
        private Series getCustomSeries(tiposGraficas tg, ChartParams_DTO chP)
        {
            switch (tg)
            {
                case tiposGraficas.Columna:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.LawnGreen;
                    chP.Serie.ChartType = SeriesChartType.Column;

                    break;


                case tiposGraficas.ColumnaT1:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Column;
                    chP.SerieP.Name = "Presupuesto Luz";
                    chP.SerieP.Color = Color.Red;
                    chP.SerieP.ChartType = SeriesChartType.Column;
                    break;


                case tiposGraficas.ColumnaT2:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Red;
                    chP.Serie.ChartType = SeriesChartType.Column;
                    break;


                case tiposGraficas.Linea:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Line;
                    break;


                case tiposGraficas.Donut:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Doughnut;
                    break;


                case tiposGraficas.Queso:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Pie;
                    break;

            }
            return chP.Serie;
        }
        private Series getCustomSeriesP(tiposGraficas tg, ChartParams_DTO chP)
        {
            switch (tg)
            {
                case tiposGraficas.Columna:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.LawnGreen;
                    chP.Serie.ChartType = SeriesChartType.Column;

                    break;


                case tiposGraficas.ColumnaT1:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Column;
                    chP.SerieP.Name = "Presupuesto Luz";
                    chP.SerieP.Color = Color.Red;
                    chP.SerieP.ChartType = SeriesChartType.Column;
                    break;


                case tiposGraficas.ColumnaT2:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Red;
                    chP.Serie.ChartType = SeriesChartType.Column;
                    break;


                case tiposGraficas.Linea:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Line;
                    break;


                case tiposGraficas.Donut:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Doughnut;
                    break;


                case tiposGraficas.Queso:
                    chP.Serie.Name = $"{chP.SerieName} - {chP.grpConcepts}";
                    chP.Serie.Color = Color.Blue;
                    chP.Serie.ChartType = SeriesChartType.Pie;
                    break;

            }
            return chP.SerieP;
        }
        private ChartArea getCustomChartArea(ChartAreaType chrtTy, ChartArea chrAr = null)
        {
            if (chrAr == null)
            {
                chrAr = new ChartArea();
            }
            switch (chrtTy)
            {
                case ChartAreaType.Type1:
                    chrAr.BackColor = Color.Black;
                    chrAr.AxisY.MajorGrid.LineColor = Color.White;
                    chrAr.AxisX.MajorGrid.LineColor = Color.White;
                    chrAr.AxisX.IsLabelAutoFit = true;
                    chrAr.AxisX.LabelStyle.Interval = 1;
                    break;
                case ChartAreaType.Type2:
                    chrAr.BackColor = Color.White;
                    chrAr.AxisY.MajorGrid.LineColor = Color.Black;
                    chrAr.AxisX.MajorGrid.LineColor = Color.Black;
                    chrAr.AxisX.IsLabelAutoFit = true;
                    chrAr.AxisX.LabelStyle.Interval = 1;
                    break;

            }
            return chrAr;
        }
        private DataPoint getCustomPoints(string x, double y)
        {
            DataPoint dp = new DataPoint();
            dp.SetValueXY(x, y);
            dp.BorderColor = Color.LightGoldenrodYellow;
            return dp;
        }
        public enum tiposGraficas
        {
            Columna,
            ColumnaT1,
            ColumnaT2,
            Linea,
            Donut,
            Queso
        }
        public enum ChartAreaType
        {
            Type1,
            Type2
        }
        #endregion
    }
}


