﻿using EconomatorApp.Helper;
using InfraestructureService;
using InfraestructureService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace EconomatorApp.Archivo
{
    public class CargaFicheroCore
    {
        FileHelper fh = new FileHelper();
        errorControl errCn = new errorControl();
        public void agregarDatosOrigenBankia(string path, string ccc, string lnCuenta, string Entidad, string fileType)
        {
            try
            {
                DateTime ultimodia = fh.ultimoDia(Entidad,false);
                DateTime ultimodiaHist = fh.ultimoDia(Entidad, true);
                string query2 = $@"DELETE FROM datosOrigenBankia WHERE fecha = '{ultimodia}' AND cccAsociada = '{ccc}'";
                string query3 = $@"DELETE FROM datosOrigenBankiaHist WHERE fecha = '{ultimodiaHist}' AND cccAsociada = '{ccc}'";

                List<string> lstOrigenTxt = new List<string>();
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {

                    isa.ExeQuery<string>(query2, null);
                    isa.ExeQuery<string>(query3, null);
                    lstOrigenTxt = fh.getTxtData(path);
                }
                //MessageBox.Show("Ultimo día borrado");

                List<DatosFuenteBankia_DTO> lstOrigenBankia = new List<DatosFuenteBankia_DTO>();
                string query = $@"INSERT INTO datosOrigenBankia(fecha,fechaValor,descripcion, importe, divisaImporte, saldo, divisaSaldo
                                                            ,concepto1,concepto2,concepto3,concepto4,concepto5,concepto6,concepto7,Entidad,LineaCuenta,cccAsociada,TipoFichero)
                                                            VALUES(@fecha,@fechaValor,@descripcion,@importe,@divisaimporte
                                                                    ,@saldo,@divisaSaldo,@concepto1,@concepto2,@concepto3,@concepto4,@concepto5,@concepto6,@concepto7,@Entidad,@LineaCuenta,@cccAsociada,@TipoFichero)";

                string queryHist = $@"INSERT INTO datosOrigenBankiaHist(fecha,fechaValor,descripcion, importe, divisaImporte, saldo, divisaSaldo
                                                            ,concepto1,concepto2,concepto3,concepto4,concepto5,concepto6,concepto7,Entidad,LineaCuenta,cccAsociada,TipoFichero)
                                                            VALUES(@fecha,@fechaValor,@descripcion,@importe,@divisaimporte
                                                                    ,@saldo,@divisaSaldo,@concepto1,@concepto2,@concepto3,@concepto4,@concepto5,@concepto6,@concepto7,@Entidad,@LineaCuenta,@cccAsociada,@TipoFichero)";

                foreach (string reg in lstOrigenTxt)
                {
                    DatosFuenteBankia_DTO dataBankia = new DatosFuenteBankia_DTO();

                    var result = reg.Split('\t');

                    dataBankia.fecha = DateTime.Parse(result[0]);
                    dataBankia.fechaValor = DateTime.Parse(result[1]);
                    dataBankia.descripcion = result[2];
                    dataBankia.importe = Convert.ToDouble(result[3]);
                    dataBankia.divisaImporte = result[4];
                    dataBankia.saldo = Convert.ToDouble(result[5]);
                    dataBankia.divisaSaldo = result[6];
                    dataBankia.concepto1 = result[7];
                    dataBankia.concepto2 = result[8];
                    dataBankia.concepto3 = result[9];
                    dataBankia.concepto4 = result[10];
                    dataBankia.concepto5 = result[11];
                    dataBankia.concepto6 = result[12];
                    dataBankia.concepto7 = result[13];
                    dataBankia.Entidad = Entidad;
                    dataBankia.LineaCuenta = lnCuenta;
                    dataBankia.cccAsociada = ccc;
                    dataBankia.TipoFichero = fileType;

                    lstOrigenBankia.Add(dataBankia);

                }
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<DatosFuenteBankia_DTO>(query, lstOrigenBankia);
                    isa.ExeQuery<DatosFuenteBankia_DTO>(queryHist, lstOrigenBankia);
                }
                MessageBox.Show($@"Datos de fichero insertados en base de datos.\n
                                    Se han insertado: {lstOrigenBankia.Count} registros", "Nuevos registros agregados", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
        }

        public void agregarDatosOrigenING(string path, string ccc, string lnCuenta, string Entidad, string fileType)
        {
            try
            {
                DateTime ultimodia = fh.ultimoDia(Entidad, false);
                DateTime ultimodiaHist = fh.ultimoDia(Entidad, true);
                string query2 = $@"DELETE FROM datosOrigenING WHERE fechaValor = '{ultimodia}' AND cccAsociada = '{ccc}'";
                string query3 = $@"DELETE FROM datosOrigenINGHist WHERE fechaValor = '{ultimodiaHist}' AND cccAsociada = '{ccc}'";

                List<string> lstOrigenTxt = new List<string>();
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {

                    isa.ExeQuery<string>(query2, null);
                    isa.ExeQuery<string>(query3, null);
                    lstOrigenTxt = fh.getExcelData(path);
                }
                //MessageBox.Show("Ultimo día borrado");

                List<DatosFuenteING_DTO> lstOrigenING = new List<DatosFuenteING_DTO>();
                string query = $@"INSERT INTO datosOrigenING(fechaValor,categoria,subcategoria,descripcion,comentario,imagen,importe,saldo, divisa,Entidad,LineaCuenta,cccAsociada,TipoFichero)
                                                            VALUES(@fechaValor,@categoria,@subcategoria,@descripcion,@comentario,@imagen,@importe,@saldo,@divisa,@Entidad,@LineaCuenta,@cccAsociada,@TipoFichero)";

                string queryHist = $@"INSERT INTO datosOrigenINGHist(fechaValor,categoria,subcategoria,descripcion,comentario,imagen,importe,saldo, divisa,Entidad,LineaCuenta,cccAsociada,TipoFichero)
                                                            VALUES(@fechaValor,@categoria,@subcategoria,@descripcion,@comentario,@imagen,@importe,@saldo,@divisa,@Entidad,@LineaCuenta,@cccAsociada,@TipoFichero)";

                foreach (string reg in lstOrigenTxt)
                {
                    DatosFuenteING_DTO dataING = new DatosFuenteING_DTO();

                    var result = reg.Split(';');

                  
                    dataING.fechaValor = Convert.ToDateTime(result[0]);
                    dataING.categoria = result[1];
                    dataING.subcategoria = result[2];
                    dataING.descripcion = result[3];
                    dataING.comentario = result[4];
                    dataING.imagen = result[5];
                    dataING.importe = Convert.ToDouble(result[6]);
                    dataING.saldo = Convert.ToDouble(result[7]);
                    dataING.divisa = "EUR";
                    dataING.Entidad = Entidad;
                    dataING.LineaCuenta = lnCuenta;
                    dataING.cccAsociada = ccc;
                    dataING.TipoFichero = fileType;

                    lstOrigenING.Add(dataING);

                }
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    isa.ExeQuery<DatosFuenteING_DTO>(query, lstOrigenING);
                    isa.ExeQuery<DatosFuenteING_DTO>(queryHist, lstOrigenING);
                }
                MessageBox.Show($@"Datos de fichero insertados en base de datos.\n
                                    Se han insertado: {lstOrigenING.Count} registros", "Nuevos registros agregados", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
        }
    }
}
