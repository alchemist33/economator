﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfraestructureService.DTOs;
using InfraestructureService;

namespace EconomatorApp.startPanel
{
    public class startPanelCore
    {
        errorControl errCn = new errorControl();
       
        public List<DatosFuenteBankia_DTO> getDatosFuente(DateTime desde, DateTime hasta, string cuenta = null, string lineaC = null)
        {
            List<DatosFuenteBankia_DTO> lstDatos = new List<DatosFuenteBankia_DTO>();
            try
            {
                string query = "";
                if (!String.IsNullOrEmpty(cuenta))
                {
                    query = $@"SELECT * FROM datosOrigenBankia
                              WHERE cccAsociada = '{cuenta}' AND Fecha BETWEEN '{desde}' AND '{hasta}' 
                              ORDER BY fecha DESC";
                }
                else
                {
                    query = $@"SELECT * FROM datosOrigenBankia
                              WHERE LineaCuenta = '{lineaC}' AND Fecha BETWEEN '{desde}' AND '{hasta}' 
                              ORDER BY fecha DESC";
                }

                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    lstDatos = isa.GetDataQuery<DatosFuenteBankia_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
            return lstDatos;
        }


        public List<DatosFuenteING_DTO> getDatosFuenteING(DateTime desde, DateTime hasta, string cuenta = null, string lineaC = null)
        {
            List<DatosFuenteING_DTO> lstDatos = new List<DatosFuenteING_DTO>();
            try
            {
                string query = "";
                if (!String.IsNullOrEmpty(cuenta))
                {
                    query = $@"SELECT * FROM datosOrigenING
                              WHERE cccAsociada = '{cuenta}' AND fechaValor BETWEEN '{desde}' AND '{hasta}' 
                              ORDER BY fechaValor DESC";
                }
                else
                {
                    query = $@"SELECT * FROM datosOrigenING
                              WHERE LineaCuenta = '{lineaC}' AND fechaValor BETWEEN '{desde}' AND '{hasta}' 
                              ORDER BY fechaValor DESC";
                }

                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    lstDatos = isa.GetDataQuery<DatosFuenteING_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
            return lstDatos;
        }
    }
}
