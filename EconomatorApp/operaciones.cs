//using Infraestructure.Common;

//using Infraestructure.Helper;
//using LiteInfraestructure;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using EconomatorApp.Helper;
using InfraestructureService;
using InfraestructureService.DTOs;

namespace EconomatorApp
{
    public class operaciones
    { //TODO: Revisar los grupos de entrada para el constructor de operaciones, crear un enum y un selector para cada vez que se invoque la clase operaciones.
        public operaciones(string loadArea = "A0")
        {
            switch (loadArea)
            {
                case "A0":
                    break;

                case "A1":
                    getConceptos();
                    getGruposConcepts();
                    break;

                case "A2":
                    getCuentas();
                    break;
                case "A2.1":
                    getCuentas();
                    getLineasCuentas();
                    break;

                case "A3":
                    getGruposConcepts();
                    break;

                case "B1":
                    getTiposPesupuestos();
                    getPresupuestos();
                    break;

                case "C1":
                    getTiposPesupuestos();
                    getPresupuestos();
                    getCuentas();
                    getConceptos();
                    getGruposConcepts();
                    break;

                case "Z1":
                    getDataBankia();
                    getTiposPesupuestos();
                    getPresupuestos();
                    break;

            }
        }


        #region Infraestructure.Common.FData

        public List<DatosFuenteBankia_DTO> lstDataBankia = new List<DatosFuenteBankia_DTO>();
        public List<TiposPresupuestos_DTO> lstTyPresu = new List<TiposPresupuestos_DTO>();
        public List<Presupuestos_DTO> lstPresu = new List<Presupuestos_DTO>();
        public List<LineaCuenta_DTO> lstLnCCCs = new List<LineaCuenta_DTO>();
        public List<cuentas_DTO> lstCCCs = new List<cuentas_DTO>();
        public List<Conceptos_DTO> lstConcepts = new List<Conceptos_DTO>();
        public List<gruposConceptos_DTO> lstGrpConcepts = new List<gruposConceptos_DTO>();

        public List<TiposPresupuestos_DTO> getTiposPesupuestos()
        {
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                this.lstTyPresu = isa.TiposPresupuestosFull();
                return lstTyPresu;
            }
        }
        public List<Presupuestos_DTO> getPresupuestos()
        {
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                this.lstPresu = isa.PresupuestosFull();
                return lstPresu;
            }
        }
        public List<cuentas_DTO> getCuentas()
        {
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                this.lstCCCs = isa.CuentasFull();
                return lstCCCs;
            }
        }
        public List<LineaCuenta_DTO> getLineasCuentas()
        {
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                this.lstLnCCCs = isa.LineasCuentasFull();
                return lstLnCCCs;
            }

            //this.lstLnCCCs = fd.LineasCuentasFull();

        }
        public List<Conceptos_DTO> getConceptos()
        {
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                this.lstConcepts = isa.ConceptosFull();
                return lstConcepts;
            }
        }
        public List<DatosFuenteBankia_DTO> getDataBankia()
        {
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                this.lstDataBankia = isa.dataBankiaFull();
                return lstDataBankia;
            }
        }
        public List<gruposConceptos_DTO> getGruposConcepts()
        {
            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                this.lstGrpConcepts = isa.gruposFull();
                return lstGrpConcepts;
            }
        }

        #endregion

        //TODO: Estos metodos tienen que pasarse a sus respectivos cores

        errorControl errCn = new errorControl();
        public double getGastoConceptoTiempo(DateTime desde, DateTime hasta, string descripcion)
        {
            double ret = 0;
            try
            {
                string query = $@"SELECT SUM(importe) FROM datosOrigenBankia 
                                WHERE descripcion LIKE '{descripcion}'
                            AND Fecha BETWEEN '{desde}' AND '{hasta}' ";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    double gastoTotal = Convert.ToDouble(isa.GetDataQuery<double>(query, null).FirstOrDefault());
                    ret = gastoTotal;
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
            return ret;
        }
        public double getNumOpsConceptoTiempo(DateTime desde, DateTime hasta, string descripcion)
        {
            double ret = 0;
            try
            {
                string query = $@"SELECT COUNT(Id) FROM datosOrigenBankia 
                                WHERE descripcion LIKE '{descripcion}'
                            AND Fecha BETWEEN '{desde}' AND '{hasta}' ";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    double gastoTotal = Convert.ToDouble(isa.GetDataQuery<double>(query, null).FirstOrDefault());
                    ret = gastoTotal;
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }


            return ret;
        }
        public List<string> getDescripcionTiempo(DateTime desde, DateTime hasta)
        {

            string query = $@"SELECT DISTINCT descripcion FROM datosOrigenBankia 
                             WHERE Fecha BETWEEN '{desde}' AND '{hasta}'  ORDER BY descripcion ASC";
            List<string> lst = new List<string>();

            using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
            {
                lst = isa.GetDataQuery<string>(query, null).ToList();
            }

            return lst;
        }
        public List<DatosFuenteBankia_DTO> getListaOpsConcepto(DateTime desde, DateTime hasta, string descripcion)
        {
            List<DatosFuenteBankia_DTO> lstBankia = new List<DatosFuenteBankia_DTO>();
            try
            {
                string query = $@"SELECT fecha, descripcion, importe, divisaImporte FROM datosOrigenBankia
                            WHERE fecha BETWEEN '{desde}' AND '{hasta}'
                            AND descripcion LIKE '{descripcion}'
                            ORDER BY fecha ASC";
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    lstBankia = isa.GetDataQuery<DatosFuenteBankia_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }

            return lstBankia;
        }
      
       
    }
}
