﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EconomatorApp
{
    public class errorControl
    {
        public enum CustomDialog
        {
            Critical,
            Error,
            ErrorLv2,
            Warning,
            Info,
        }
        private void ShowMsg(string mensaje, string caption, MessageBoxButtons botones, MessageBoxIcon icon)
        {
            MessageBox.Show(mensaje, caption, botones, icon);
        }

        #region Niveles de detalle de error
        private string ErrorDetail1(Exception ex, string extraMsg = "")
        {
            string ret = $"Error del tipo: {ex.GetType()}.\nData: {ex.Data}.\nMensaje: {ex.Message}\n{extraMsg}";
            return ret;
        }
        private string ErrorDetail2(Exception ex, string extraMsg = "")
        {
            string ret = $"Error del tipo: {ex.GetType()}.\nData: {ex.Data}.\nMensaje: {ex.Message}\n{extraMsg}";
            return ret;
        }
        #endregion

        public void errorSelector(CustomDialog typeSelect, Exception ex, string extraMsg = "")
        {
            switch (typeSelect)
            {
                case CustomDialog.Error:
                    ShowMsg(ErrorDetail1(ex, extraMsg), "Error de aplicación", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                    case CustomDialog.ErrorLv2:
                    ShowMsg(ErrorDetail2(ex, extraMsg), "Error de aplicación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;

            }
        }
    }
}
