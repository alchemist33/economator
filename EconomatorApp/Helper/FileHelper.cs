﻿using InfraestructureService;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EconomatorApp.Helper
{
    public class FileHelper
    {
        errorControl errCn = new errorControl();
        public DateTime ultimoDia(string entidad, bool historico)
        {
            DateTime ret = new DateTime(1987, 01, 01);
            string query = "";
            switch (entidad)
            {
                case "BANKIA":
                    if (historico)
                    {
                        query = "SELECT MAX(fecha) FROM datosOrigenBankiaHist";
                    }
                    else
                    {
                        query = "SELECT MAX(fecha) FROM datosOrigenBankia";
                    }

                    break;
                case "ING":
                    if (historico)
                    {
                        query = "SELECT MAX(fechaValor) FROM datosOrigenINGHist";
                    }
                    else
                    {
                        query = "SELECT MAX(fechaValor) FROM datosOrigenING";
                    }

                    break;
            }

            try
            {
                using (InfraestructureServiceAccess isa = new InfraestructureServiceAccess())
                {
                    ret = isa.GetDataQuery<DateTime>(query, null).FirstOrDefault();
                }
                if (ret == null)
                {
                    ret = DateTime.Now.Date.AddYears(-100);
                }
            }
            catch (Exception ex)
            {
                errCn.errorSelector(errorControl.CustomDialog.Error, ex, null);

            }

            return ret.Date;
        }



        public List<string> getTxtData(string path)
        {
            List<string> txtData = new List<string>();
            StreamReader sr = new StreamReader(path);
            string texto;
            for (int i = 0; i <= 5; i++)
            {
                sr.ReadLine();
            }

            while ((texto = sr.ReadLine()) != null)
            {
                txtData.Add(texto);
            }
            return txtData;

        }

        public List<string> getExcelData(string path)
        {
            List<string> ExcelData = new List<string>();


            FileInfo newFile = new FileInfo(path);
            using (ExcelPackage pck = new ExcelPackage(newFile))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets[0];
                ExcelCellAddress startCell = ws.Dimension.Start;
                ExcelCellAddress endCell = ws.Dimension.End;

                for (int row = startCell.Row; row <= endCell.Row; row++)
                {
                    var line = "";
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        var temp = "";
                        if (row > 6)
                        {
                            if (ws.Cells[row, col].Value != null)
                            {
                                temp = ws.Cells[row, col].Value.ToString();
                            }

                            line += temp + ";";

                        }
                    }
                    if (!String.IsNullOrEmpty(line))
                    {
                        ExcelData.Add(line);
                    }
                }
            }
            return ExcelData;
        }
    }
}
