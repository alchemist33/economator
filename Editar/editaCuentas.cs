﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorAppService;
using EconomatorAppService.DTOs;

namespace Economator.Editar
{
    public partial class editaCuentas : Form
    {
       public static cuentas_DTO cccTransfer = new cuentas_DTO();

        public editaCuentas()
        {
            InitializeComponent();
        }
        public void refreshCCCs(bool operativas = true)
        {
            

            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                if (!operativas)
                {
                    dataGridView1.DataSource = easa.getCuentas2(true);
                }
                else
                {
                    dataGridView1.DataSource = easa.getCuentas2();

                }
            }

            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            nuevaCuenta nc = new nuevaCuenta();
            nc.Show();
        
            
        }

        private void EditaCuentas_Load(object sender, EventArgs e)
        {

            refreshCCCs(false);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.SelectedCells[0].Value.ToString();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess() )
            {
                easa.deleteCuenta(Guid.Parse(id));
            }
               
            if (checkBox1.Checked)
            {
                refreshCCCs();
            }
            else
            {
                refreshCCCs(false);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                refreshCCCs();
            }
            else
            {
                refreshCCCs(false);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
            cccTransfer.idCuenta = Guid.Parse(dataGridView1.SelectedCells[0].Value.ToString());
            cccTransfer.LineaCuenta = dataGridView1.SelectedCells[1].Value.ToString();
            cccTransfer.nombreCCC = dataGridView1.SelectedCells[2].Value.ToString();
            cccTransfer.entidadCCC = dataGridView1.SelectedCells[3].Value.ToString();
            cccTransfer.numeroCCC = dataGridView1.SelectedCells[4].Value.ToString();
            cccTransfer.operativa = bool.Parse(dataGridView1.SelectedCells[5].Value.ToString());
            cccTransfer.fecha_alta = DateTime.Parse( dataGridView1.SelectedCells[6].Value.ToString());
            cccTransfer.fecha_baja = DateTime.Parse(dataGridView1.SelectedCells[7].Value.ToString());
            cccTransfer.comentarios = dataGridView1.SelectedCells[8].Value.ToString();

            updateCuenta uc = new updateCuenta();
            uc.Show();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked)
            {
                refreshCCCs();
            } else
            {
                refreshCCCs(false);
            }
            
        }
    }
}
