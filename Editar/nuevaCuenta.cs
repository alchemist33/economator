﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EconomatorAppService;
using EconomatorAppService.DTOs;

namespace Economator.Editar
{
    public partial class nuevaCuenta : Form
    {
        //private editaCuentasCore edtCuentasCore = new editaCuentasCore();
        //private OpcionesCore optCore = new OpcionesCore();
        public nuevaCuenta()
        {
            InitializeComponent();
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            cuentas_DTO ccc = new cuentas_DTO();
            ccc.nombreCCC = textBox1.Text;
            ccc.entidadCCC = comboBox2.Text;
            ccc.numeroCCC = textBox3.Text;
            ccc.operativa = checkBox1.Checked;
            ccc.fecha_alta = dateTimePicker1.Value;
            ccc.fecha_baja = dateTimePicker2.Value;
            ccc.comentarios = textBox4.Text;

            ccc.LineaCuenta = comboBox1.Text;
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                easa.nuevaCuenta(ccc);
            }
               // edtCuentasCore.nuevaCuenta(ccc);
            this.Close();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                dateTimePicker2.Enabled = true;
            }
            else
            {
                dateTimePicker2.Enabled = false;
            }
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            if (comboBox1.Items != null && comboBox1.Items.Count > 0)
            {
                comboBox1.Items.Clear();
            }

            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                foreach (var reg in easa.getOptionsValues("LineasCuentas"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox1.Items.Add(reg);
                    }
                }
            }
        }

        private void nuevaCuenta_Load(object sender, EventArgs e)
        {
           
        }

        private void comboBox2_DropDown(object sender, EventArgs e)
        {
            if (comboBox2.Items != null && comboBox2.Items.Count > 0)
            {
                comboBox2.Items.Clear();
            }
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                foreach (var reg in easa.getOptionsValues("Entidades"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox2.Items.Add(reg);
                    }

                }
            }
        }
    }
}
