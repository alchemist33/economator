﻿
using EconomatorAppService.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorAppService;

namespace Economator.Editar
{
    public partial class Opciones : Form
    {
        //OpcionesCore optCore = new OpcionesCore();
        public Opciones()
        {
            InitializeComponent();
        }

        private void Opciones_Load(object sender, EventArgs e)
        {

        }
        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string opcion = treeView1.SelectedNode.Text;
            switch (opcion)
            {
                case "Ficheros":
                    options_Ficheros.Show();
                    options_LineasCuenta.Hide();
                    refreshEntidades();
                    refreshFileTypes();

                    break;
                case "Lineas de Cuenta":
                    options_Ficheros.Hide();
                    options_LineasCuenta.Show();
                    refreshLineasCuentas();
                    break;


            }
        }

        #region Ids & Name Options
        string opEnti = "Entidades";
        string idOptEntidades = "B1ABDA70-EB74-46D9-B62C-BFA562A257EE";
        string opLinCuenta = "LineasCuentas";
        string idOptLnCuenta = "A40D5E24-9649-4D70-A835-AA988A5F4706";
        string opFileType = "TiposFichero";
        string idOptFileType = "FD375656-65AC-4C6B-85B4-ECC52E3D545C";


        #endregion

        #region Opciones de lineas de cuentas
        private void refreshLineasCuentas()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox1.Items.Clear();
                foreach (var reg in easa.getOptionsValues(opLinCuenta))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        listBox1.Items.Add(reg);
                    }

                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {

            Opciones_DTO option = new Opciones_DTO();
            option.idOption = Guid.Parse(idOptLnCuenta);
            option.optionName = opLinCuenta;
            List<string> lnsCuenta = new List<string>();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                lnsCuenta = easa.getOptionsValues(opLinCuenta);
            }
            lnsCuenta.RemoveAt(lnsCuenta.Count - 1);
            lnsCuenta.Add(textBox1.Text.ToUpper());

            string values = "";
            foreach (var reg in lnsCuenta)
            {
                values += $"{reg};";
            }

            option.optionValue = values;
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                easa.updateOptionsValues(option);
            }
            //  optCore.updateValorOpcion(option);
            MessageBox.Show("Linea de cuenta agregada correctamente");
            refreshLineasCuentas();
            textBox1.Text = "";
            textBox2.Text = "";

        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                textBox2.Text = listBox1.SelectedItem.ToString();
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Opciones_DTO option = new Opciones_DTO();
            option.idOption = Guid.Parse(idOptLnCuenta);
            option.optionName = opLinCuenta;

            List<string> lnsCuenta = new List<string>();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                lnsCuenta = easa.getOptionsValues(opLinCuenta);
            }

            lnsCuenta.RemoveAt(lnsCuenta.Count - 1);
            for (int i = 0; i < lnsCuenta.Count; i++)
            {
                if (listBox1.SelectedItem.ToString() == lnsCuenta[i])
                {
                    lnsCuenta[i] = textBox2.Text.ToUpper(); ;
                }
            }
            string values = "";
            foreach (var reg in lnsCuenta)
            {
                values += $"{reg};";
            }

            option.optionValue = values;
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                easa.updateOptionsValues(option);
            }
            MessageBox.Show("Linea de cuenta actualizada correctamente");
            refreshLineasCuentas();
            textBox1.Text = "";
            textBox2.Text = "";


        }
        private void button3_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                Opciones_DTO option = new Opciones_DTO();
                option.idOption = Guid.Parse(idOptLnCuenta);
                List<string> lnsCuenta = easa.getOptionsValues(opLinCuenta);
                string values = "";
                for (int i = 0; i < lnsCuenta.Count; i++)
                {
                    if (listBox1.SelectedItem.ToString() != lnsCuenta[i] && !String.IsNullOrEmpty(lnsCuenta[i]))
                    {
                        values += $"{lnsCuenta[i]};";
                    }
                }
                option.optionValue = values;
                easa.updateOptionsValues(option);
                MessageBox.Show("Linea de cuenta eliminada correctamente");
                refreshLineasCuentas();
                textBox1.Text = "";
                textBox2.Text = "";
            }

        }
        #endregion

        #region Opciones de Entidades
        private void refreshEntidades()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox2.Items.Clear();
                foreach (var reg in easa.getOptionsValues(opEnti))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        listBox2.Items.Add(reg);
                    }

                }
            }
        }
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string enti = "";
            if (listBox2.SelectedItem != null)
            {
                enti = listBox2.SelectedItem.ToString();
            }
            textBox3.Text = enti;

        }
        private void button5_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                Opciones_DTO option = new Opciones_DTO();
                option.idOption = Guid.Parse(idOptEntidades);
                option.optionName = opEnti;

                List<string> entis = easa.getOptionsValues(opEnti);
                entis.RemoveAt(entis.Count - 1);
                entis.Add(txtNuevaEntidad.Text.ToUpper());

                string values = "";
                foreach (var reg in entis)
                {
                    values += $"{reg};";
                }

                option.optionValue = values;
                easa.updateOptionsValues(option);
                MessageBox.Show("Entidad agregada correctamente");
                refreshEntidades();
                txtNuevaEntidad.Text = "";
                textBox3.Text = "";
            }

        }
        private void button6_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                Opciones_DTO option = new Opciones_DTO();
                option.idOption = Guid.Parse(idOptEntidades);
                option.optionName = opEnti;

                List<string> entis = easa.getOptionsValues(opEnti);
                entis.RemoveAt(entis.Count - 1);
                for (int i = 0; i < entis.Count; i++)
                {
                    if (listBox2.SelectedItem.ToString() == entis[i])
                    {
                        entis[i] = textBox3.Text.ToUpper(); ;
                    }
                }
                string values = "";
                foreach (var reg in entis)
                {
                    values += $"{reg};";
                }

                option.optionValue = values;
                easa.updateOptionsValues(option);
                MessageBox.Show("Entidad actualizada correctamente");
                refreshEntidades();
                textBox3.Text = "";
                txtNuevaEntidad.Text = "";
            }

        }
        private void button4_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                Opciones_DTO option = new Opciones_DTO();
                option.idOption = Guid.Parse(idOptEntidades);
                List<string> entis = easa.getOptionsValues(opEnti);
                string values = "";
                for (int i = 0; i < entis.Count; i++)
                {
                    if (listBox2.SelectedItem.ToString() != entis[i] && !String.IsNullOrEmpty(entis[i]))
                    {
                        values += $"{entis[i]};";
                    }
                }
                option.optionValue = values;
                easa.updateOptionsValues(option);
                MessageBox.Show("Entidad eliminada correctamente");
                refreshEntidades();
                textBox3.Text = "";
                txtNuevaEntidad.Text = "";
            }
        }
        #endregion


        #region Opciones de Tipos de fichero
        private void refreshFileTypes()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox3.Items.Clear();
                foreach (var reg in easa.getOptionsValues(opFileType))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        listBox3.Items.Add(reg);
                    }

                }
            }
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox3.SelectedItem != null)
            {
                textBox4.Text = listBox3.SelectedItem.ToString();
            }

        }
        private void button8_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                Opciones_DTO option = new Opciones_DTO();
                option.idOption = Guid.Parse(idOptFileType);
                option.optionName = opFileType;

                List<string> entis = easa.getOptionsValues(opFileType);
                entis.RemoveAt(entis.Count - 1);
                entis.Add(textBox5.Text.ToUpper());

                string values = "";
                foreach (var reg in entis)
                {
                    values += $"{reg};";
                }

                option.optionValue = values;
                easa.updateOptionsValues(option);
                MessageBox.Show("Tipo agregado correctamente");
                refreshFileTypes();
                textBox4.Text = "";
                textBox5.Text = "";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                Opciones_DTO option = new Opciones_DTO();
                option.idOption = Guid.Parse(idOptFileType);
                option.optionName = opFileType;

                List<string> entis = easa.getOptionsValues(opFileType);
                entis.RemoveAt(entis.Count - 1);
                for (int i = 0; i < entis.Count; i++)
                {
                    if (listBox3.SelectedItem.ToString() == entis[i])
                    {
                        entis[i] = textBox4.Text.ToUpper(); ;
                    }
                }
                string values = "";
                foreach (var reg in entis)
                {
                    values += $"{reg};";
                }

                option.optionValue = values;
                easa.updateOptionsValues(option);
                MessageBox.Show("Tipo actualizado correctamente");
                refreshFileTypes();
                textBox4.Text = "";
                textBox5.Text = "";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                Opciones_DTO option = new Opciones_DTO();
                option.idOption = Guid.Parse(idOptFileType);
                List<string> entis = easa.getOptionsValues(opFileType);
                string values = "";
                for (int i = 0; i < entis.Count; i++)
                {
                    if (listBox3.SelectedItem.ToString() != entis[i] && !String.IsNullOrEmpty(entis[i]))
                    {
                        values += $"{entis[i]};";
                    }
                }
                option.optionValue = values;
                easa.updateOptionsValues(option);
                MessageBox.Show("Tipo eliminado correctamente");
                refreshFileTypes();
                textBox4.Text = "";
                textBox5.Text = "";
            }
        }

        #endregion


    }
}
