﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorAppService;

namespace Economator.Editar
{
    public partial class editaPresupuestos : Form
    {
        public editaPresupuestos()
        {
            InitializeComponent();
        }
        private void refreshTipos()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox3.Items.Clear();
                foreach (var reg in easa.getNombresTipoPresupuesto())
                {
                    listBox3.Items.Add(reg);
                }
            }
        }
        private void refreshImportes(string nomTipoPresu)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = easa.getTiposPresupuestosFull().Where(x => x.IdTipoPresupuesto == easa.getIdTipoPresu(nomTipoPresu)).ToList();

            }
        }
        private void refreshPresupuestos()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox1.Items.Clear();
                foreach (var reg in easa.getNombresPresupuesto())
                {
                    listBox1.Items.Add(reg);
                }
            }
        }
        private void EditaPresupuestos_Load(object sender, EventArgs e)
        {
            refreshTipos();
            refreshPresupuestos();
        }
        private void ComboBox1_DropDown(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                comboBox1.Items.Clear();
                foreach (var reg in easa.getNombresTipoPresupuesto())
                {
                    comboBox1.Items.Add(reg);
                }
            }
        }
        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                button4.Enabled = false;
                button3.Text = "Agregar tipo presupuesto";

            }
            else
            {
                button4.Enabled = true;
                button3.Text = "Actualizar tipo presupuesto";
            }
            refreshTipos();
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                string new_nomPresu = textBox1.Text;
                string old_nomPresu = label4.Text;
                string nomTipoPresu = "";
                bool sumaImportes = checkBox3.Checked;

                if (comboBox1.Text != null)
                {
                    nomTipoPresu = comboBox1.Text;
                }
                easa.updatePresupuesto(new_nomPresu, old_nomPresu, nomTipoPresu, sumaImportes);

                comboBox1.Items.Clear();
                textBox1.Text = String.Empty;
                comboBox1.Text = String.Empty;
                label4.Text = String.Empty;
                label8.Text = String.Empty;
                refreshPresupuestos();
            }
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                if (button3.Text == "Agregar tipo presupuesto")
                {
                    easa.nuevoTipoPresupuesto(textBox2.Text);
                }
                else
                {
                    easa.updateTipoPresupuesto(textBox2.Text, label1.Text);
                }
                checkBox2.Checked = false;
                textBox2.Text = "";
                refreshTipos();
            }
        }
        private void Button4_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                string tipo = label1.Text;
                DialogResult result = new DialogResult();
                result = MessageBox.Show($@"Atención! Seguro que deseas borrar el Tipo de presupuesto -{tipo}-??
                               Esta acción no se peude deshacer.
                               Todos los presupuestos asignados a este Tipo perdaran la vinvulacion con el tipo", "Borrado de Tipo de presupuesto", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    easa.deleteTipoPresupuesto(tipo);
                    refreshTipos();
                    label1.Text = "...";
                }
            }
        }
        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                string nomPresu = listBox1.SelectedItem.ToString();
                label4.Text = nomPresu;
                textBox1.Text = nomPresu;
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    label8.Text = easa.getTipoPresupuesto(easa.getPresupuestosFull().Where(x => x.nombrePresupuesto == nomPresu).Select(x => x.IdTipoPresupuesto).FirstOrDefault()).nombreTipo;
                    var  res = easa.getPresupuestosFull().Where(x => x.nombrePresupuesto == nomPresu).Select(x => x.sumaImportes).FirstOrDefault();
                    checkBox3.Checked = res;
                    comboBox1.Text = label8.Text;
                }

            }
        }
        private void ListBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox3.SelectedItem != null)
            {
                label1.Text = listBox3.SelectedItem.ToString();
                refreshImportes(listBox3.SelectedItem.ToString());
            }

        }
        private void button5_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                if (listBox3.SelectedItem != null)
                {
                    if (!String.IsNullOrEmpty(textBox3.Text))
                    {
                        easa.nuevoImporte(listBox3.SelectedItem.ToString(), double.Parse(textBox3.Text));
                        refreshImportes(listBox3.SelectedItem.ToString());
                        textBox3.Text = String.Empty;
                    }
                    else
                    {
                        MessageBox.Show("Indica un importe");
                    }

                }
                else
                {
                    MessageBox.Show("Selecciona un Tipo de presupuesto");
                }
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                if (listBox3.SelectedItem != null)
                {
                    string importeN = $"importe{dataGridView1.CurrentCell.ColumnIndex - 1}";
                    if (!String.IsNullOrEmpty(textBox4.Text))
                    {
                        easa.updateImporte(listBox3.SelectedItem.ToString(), double.Parse(textBox4.Text), importeN);
                        refreshImportes(listBox3.SelectedItem.ToString());
                        textBox4.Text = String.Empty;
                    }
                    else
                    {
                        MessageBox.Show("Indica un nuevo importe");
                    }
                }
                else
                {
                    MessageBox.Show("Selecciona un Tipo de presupuesto");
                }
            }
        }
        private void button7_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                if (listBox3.SelectedItem != null)
                {
                    string importeN = $"importe{dataGridView1.CurrentCell.ColumnIndex - 1}";

                    easa.deleteImporte(listBox3.SelectedItem.ToString(), importeN);
                    refreshImportes(listBox3.SelectedItem.ToString());
                }
                else
                {
                    MessageBox.Show("Selecciona un Tipo de presupuesto");
                }
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
            checkBox4.Checked = false;
            textBox5.Text = string.Empty;
            comboBox2.Text = string.Empty;
        }
        private void button9_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                panel1.Visible = false;
                panel2.Visible = true;
            }
            else
            {
                MessageBox.Show("Selecciona un presupuesto para editar");
            }
        }
        private void button11_Click(object sender, EventArgs e)
        {
            string newPresu =string.Empty;
            string nomTipoPresu = string.Empty;
            bool sumaImportes = checkBox4.Checked;
            if (!String.IsNullOrEmpty(textBox5.Text))
            {
                newPresu = textBox5.Text;
                if (!String.IsNullOrEmpty(comboBox2.Text))
                {
                    nomTipoPresu = comboBox2.Text;
                    using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                    {
                        Guid idTipoPresu = easa.getIdTipoPresu(nomTipoPresu);
                        easa.nuevoPresupuesto(newPresu, nomTipoPresu, idTipoPresu, sumaImportes);
                        refreshPresupuestos();
                        textBox5.Text = string.Empty;
                        comboBox2.Text = string.Empty;
                    }
                }
                else
                {
                    MessageBox.Show("Selecciona un Tipo de presupuesto");
                }
            }
            else
            {
                MessageBox.Show("Indica un nombre para el presupuesto");
            }
        }
        private void button10_Click(object sender, EventArgs e)
        {
            string presu = label4.Text;
            DialogResult result = new DialogResult();
            result = MessageBox.Show($@"Atención! Seguro que deseas borrar el presupuesto -{presu}-??
                               Esta acción no se puede deshacer.", "Borrado de Presupuesto", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.Yes)
            {
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    easa.deletePresupuesto(presu);
                    refreshPresupuestos();
                    label4.Text = "...";
                    label8.Text = "...";
                }
            }
        }
        private void comboBox2_DropDown(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                comboBox2.Items.Clear();
                foreach (var reg in easa.getNombresTipoPresupuesto())
                {
                    comboBox2.Items.Add(reg);
                }
            }
        }
    }
}
