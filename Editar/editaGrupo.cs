﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorAppService;


namespace Economator.Editar
{

    public partial class editaGrupo : Form
    {
        //TODO: Hay que crear subgrupos
        //errorControl errCn = new errorControl();
        //editaConcpetosCore edConCore = new editaConcpetosCore();
        public editaGrupo()
        {
            InitializeComponent();
        }

        private void EditaGrupo_Load(object sender, EventArgs e)
        {
            refreshGrupos();
        }
        private string getGrupo()
        {
            string ret = listBox1.SelectedItem.ToString();
            return ret;
        }
        private void refreshGrupos()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox1.Items.Clear();
                var lst = easa.getListGroupConcepts();
                foreach (var reg in lst)
                {
                    listBox1.Items.Add(reg);
                }
                label3.Text = listBox1.Items.Count.ToString();
            }
        }
        private void updateConceptos()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox2.Items.Clear();
                var lst = easa.getConceptosGrupo(getGrupo());
                foreach (var reg in lst)
                {
                    listBox2.Items.Add(reg);
                }
                label6.Text = listBox2.Items.Count.ToString();
            }
        }
        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                label9.Text = listBox1.SelectedItem.ToString();
                updateConceptos();
            }

        }
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                button2.Enabled = false;
                button1.Text = "Crear grupo";
                label9.Text = "Nuevo grupo";
            }
            else
            {
                button2.Enabled = true;
                button1.Text = "Actualizar grupo";
                label9.Text = "...";
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            string grupoOld = label9.Text;
            string grupoNew = textBox1.Text;
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                if (button1.Text == "Actualizar grupo")
                {
                    if (easa.updateGrupo(grupoNew, grupoOld))
                    {
                        MessageBox.Show($"El grupo {grupoOld} a pasado a llamarse {grupoNew}", "Nombre de grupo actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        refreshGrupos();
                        label9.Text = "...";
                    }
                }
                else //crear nuevo grupo
                {
                    easa.nuevoGrupo(grupoNew);
                    refreshGrupos();
                    label9.Text = "...";
                    button2.Enabled = true;
                    checkBox1.Checked = false;
                    textBox1.Text = "";
                }
            }

        }
        private void Button2_Click(object sender, EventArgs e)
        {
            string grupo = label9.Text;
            DialogResult result = new DialogResult();
            if (grupo == "...")
            {
                MessageBox.Show("Seleccion un grupo para eliminar");
            }
            else
            {
                result = MessageBox.Show($@"Atención! Seguro que deseas borrar el grupo -{grupo}-??
                               Esta acción no se peude deshacer.
                               Todos los conceptos asignados a este grupo perdaran la vinvulacion con el grupo", "Borrado de grupo", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                    {
                        easa.deleteGrupo(grupo);
                        refreshGrupos();
                        label9.Text = "...";
                    }

                }
            }

        }
    }
}
