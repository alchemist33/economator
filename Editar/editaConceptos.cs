﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorAppService;
using EconomatorAppService.DTOs;

namespace Economator.Editar
{
    public partial class editaConceptos : Form
    {
        //TODO: Los conceptos tienen que poder asignarse a varios grupos y subgrupos
        //editaConcpetosCore edConCore = new editaConcpetosCore();
        //operaciones op = new operaciones("A3");
        //errorControl errCn = new errorControl();
        EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess();
        public editaConceptos()
        {
            InitializeComponent();
        }

        #region Updates
        private void updateGrupos()
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox2.Items.Clear();
                var lst = easa.getListGroupConcepts();

                foreach (var reg in lst)
                {
                    listBox2.Items.Add(reg);
                }
            }
        }
        private void updateConceptos()
        {

            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox1.Items.Clear();
                var lst = easa.getConcepts();
                foreach (var reg in lst)
                {

                    listBox1.Items.Add(reg);

                }
            }

        }
        private void updateConceptosSinGrupo()
        {

            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox1.Items.Clear();
                var lst = easa.getConceptsSinGrupo();
                foreach (var reg in lst)
                {
                    listBox1.Items.Add(reg);
                }
            }
        }
        private void updateGruposConcepto()
        {
            try
            {
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    listBox3.Items.Clear();
                    string concepto = listBox1.SelectedItem.ToString();
                    var lst = easa.getConceptosGrupo(concepto);
                    if (lst.Select(x => x != null).FirstOrDefault())
                    {
                        foreach (var reg in lst)
                        {
                            listBox3.Items.Add(reg);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //errCn.errorSelector(errorControl.CustomDialog.Error, ex,"Localizador: Ec.ConfigConceptos.updateGruposConcepto()");
            }
        }
        private void updateConceptosGrupo()
        {
            try
            {
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    listBox4.Items.Clear();
                    string grupo = listBox2.SelectedItem.ToString();
                    var lst = easa.getConceptosGrupo(grupo);

                    foreach (var reg in lst)
                    {
                        listBox4.Items.Add(reg);
                    }
                }

            }
            catch (Exception ex)
            {
                // errCn.errorSelector(errorControl.CustomDialog.Error, ex);
            }
        }
        #endregion

        #region Contadores
        public void updateContadores()
        {
            label4.Text = listBox1.Items.Count.ToString();
            label6.Text = listBox2.Items.Count.ToString();
            label7.Text = listBox3.Items.Count.ToString();
            label8.Text = listBox4.Items.Count.ToString();
        }
        #endregion

        private void ConfigConceptos_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            updateConceptos();
            updateGrupos();
            updateContadores();
        }
        private void Button5_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                easa.updateConceptos();
                updateConceptos();
                updateContadores();
            }


        }
        private void Button8_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                string nombreGrupo = listBox2.SelectedItem.ToString();
                easa.deleteGrupo(nombreGrupo);
                updateContadores();
            }
        }


        /// <summary>
        /// Asigancion de un concpeto a un grupo de conceptos
        /// <code>Button1 => Asignar </code>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {

            string concepto = "";
            string grupo = "";
            if (listBox1.SelectedItem == null || listBox2.SelectedItem == null)
            {
                MessageBox.Show("Selecciona un CONCEPTO y un GRUPO");
            }
            else
            {
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    concepto = listBox1.SelectedItem.ToString();
                    grupo = listBox2.SelectedItem.ToString();
                    string gruposPrev = "";
                    string newGroups = "";
                    if (easa.getGruposConcepto(concepto).Select(x => x.grupoConcepto).First() != null)
                    {
                        gruposPrev = easa.getGruposConcepto(concepto).Select(x => x.grupoConcepto).First().ToString();
                        newGroups = $"{gruposPrev}{grupo};";
                    }
                    else
                    {
                        newGroups = $"{grupo};";
                    }

                    //newGroups = $"{gruposPrev};{grupo};";
                    easa.asignarGrupo(concepto, newGroups);
                    if (radioButton1.Checked == false && radioButton2.Checked == true)
                    {
                        updateConceptosSinGrupo();
                    }
                    else
                    {
                        updateConceptos();
                    }
                    updateConceptosGrupo();
                    updateContadores();
                    label10.Text = string.Empty;
                }
            }
        }
        private void ListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateConceptosGrupo();
            updateContadores();
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            updateGrupos();
            updateContadores();
        }
        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == false && radioButton2.Checked == true)
            {
                updateConceptosSinGrupo();
            }
            else
            {
                updateConceptos();
            }
            updateContadores();
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                string concepto = listBox4.SelectedItem.ToString();
                string grupo = listBox2.SelectedItem.ToString();
                string gruposPrev = easa.getGruposConcepto(concepto).Select(x => x.grupoConcepto).First().ToString();
                string newGroups = "";
                foreach (var reg in gruposPrev.Split(';'))
                {
                    if (reg != grupo && !String.IsNullOrEmpty(reg))
                    {
                        newGroups += $"{reg};";
                    }
                }

                easa.quitarGrupo(concepto, newGroups);
                if (radioButton1.Checked == false && radioButton2.Checked == true)
                {
                    updateConceptosSinGrupo();
                }
                else
                {
                    updateConceptos();
                }
                updateConceptosGrupo();
                updateContadores();
            }
        }
        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string concepto = "";
            if (listBox1.SelectedItem != null)
            {
                updateGruposConcepto();
                updateContadores();
                concepto = listBox1.SelectedItem.ToString();
                label10.Text = concepto;
                using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
                {
                    if (easa.getGruposConcepto(concepto).First().grupoConcepto != null && easa.getGruposConcepto(concepto).Count > 0)
                    {
                        var lst = easa.getGruposConcepto(concepto).First().grupoConcepto.ToString(); ;
                        foreach (var reg in lst.Split(';'))
                        {
                            if (!String.IsNullOrEmpty(reg))
                            {
                                listBox3.Items.Add(reg);
                            }
                        }
                    }

                }
                getDetalle(concepto);
            }
           
            label7.Text = listBox3.Items.Count.ToString();
           
        }

        private void getDetalle(string concepto)
        {
            label15.Text = "";
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                var lst = easa.getDetalleConceptoBankia(concepto).OrderBy(x => x.fecha).ToList();
                if (lst != null && lst.Count > 0)
                {
                    if (lst.Count > 1)
                    {
                        foreach (var reg in lst)
                        {
                            if (!label15.Text.Contains(reg.cccAsociada))
                            {
                                label15.Text += $@" {reg.cccAsociada} \";
                            }
                        }
                    }
                    else
                    {
                        label15.Text = lst[0].cccAsociada;
                    }

                    label16.Text = lst.Count.ToString();
                    label17.Text = lst.First().fecha.ToString();
                    label18.Text = lst.Last().fecha.ToString();
                    label19.Text = $"{-lst.Sum(x => x.importe) / lst.Count} €";
                    label21.Text = $"{lst.Max(x => -x.importe)} €";
                    label23.Text = $"{lst.Min(x => -x.importe)} €";
                }
                else
                {
                    var lst2 = easa.getDetalleConceptoING(concepto).OrderBy(x => x.fechaValor).ToList();
                    if (lst2 != null && lst2.Count > 0)
                    {
                        if (lst2.Count > 1)
                        {
                            foreach (var reg in lst2)
                            {
                                if (!label15.Text.Contains(reg.cccAsociada))
                                {
                                    label15.Text += $@" {reg.cccAsociada} \";
                                }
                            }
                        }
                        else
                        {
                            label15.Text = lst2[0].cccAsociada;
                        }

                        label16.Text = lst2.Count.ToString();
                        label17.Text = lst2.First().fechaValor.ToString();
                        label18.Text = lst2.Last().fechaValor.ToString();
                        label19.Text = $"{-lst2.Sum(x => x.importe) / lst2.Count} €";
                        label21.Text = $"{lst2.Max(x => -x.importe)} €";
                        label23.Text = $"{lst2.Min(x => -x.importe)} €";
                    }
                }
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {


        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {

            string busqueda = textBox1.Text;
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                listBox1.Items.Clear();
                foreach (var reg in easa.BuscaConceptos(busqueda))
                {
                    listBox1.Items.Add(reg);
                }
                updateContadores();

            }
        }
    }
}
