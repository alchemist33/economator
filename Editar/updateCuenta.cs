﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorAppService;
using EconomatorAppService.DTOs;

namespace Economator.Editar
{
    public partial class updateCuenta : Form
    {
        public updateCuenta()
        {
            InitializeComponent();
        }

        private void updateCuenta_Load(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                comboBox1.Text = easa.getOptionsValues("LineasCuentas").Where(x => x == editaCuentas.cccTransfer.LineaCuenta).FirstOrDefault().ToString();//.Select(x => x.NombreLineaCuenta).FirstOrDefault().ToString();
                checkBox1.Checked = editaCuentas.cccTransfer.operativa;
                textBox1.Text = editaCuentas.cccTransfer.nombreCCC;
                comboBox2.Text = editaCuentas.cccTransfer.entidadCCC;
                textBox3.Text = editaCuentas.cccTransfer.numeroCCC;
                textBox4.Text = editaCuentas.cccTransfer.comentarios;
                dateTimePicker1.Value = editaCuentas.cccTransfer.fecha_alta;
                dateTimePicker2.Value = editaCuentas.cccTransfer.fecha_baja;
            }
            
           
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                dateTimePicker2.Enabled = true;
            }
            else
            {
                dateTimePicker2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        

            cuentas_DTO ccc = new cuentas_DTO();
            ccc.idCuenta = editaCuentas.cccTransfer.idCuenta;
            ccc.LineaCuenta = comboBox1.Text;
            ccc.operativa = checkBox1.Checked ;
            ccc.nombreCCC = textBox1.Text ;
            ccc.entidadCCC = comboBox2.Text ;
            ccc.numeroCCC = textBox3.Text ;
            ccc.comentarios = textBox4.Text ;
            ccc.fecha_alta = dateTimePicker1.Value;
            ccc.fecha_baja = dateTimePicker2.Value;
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                easa.updateCuenta(ccc, editaCuentas.cccTransfer.nombreCCC);
            }
              
            this.Close();

        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {

            comboBox1.Items.Clear();
           
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                foreach (var reg in easa.getOptionsValues("LineasCuentas"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox1.Items.Add(reg);
                    }
                }
            }
                
        }

        private void comboBox2_DropDown(object sender, EventArgs e)
        {
            if (comboBox2.Items != null && comboBox2.Items.Count > 0)
            {
                comboBox2.Items.Clear();
            }
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                foreach (var reg in easa.getOptionsValues("Entidades"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox2.Items.Add(reg);
                    }

                }
            }
        }
    }
}
