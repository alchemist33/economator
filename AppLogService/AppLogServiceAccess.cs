﻿using AppLog;
using AppLog.DTO;
using AppLogService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLogService
{
    public class AppLogServiceAccess : IDisposable
    {
        public void Dispose()
        {
            this.Dispose();
        }
        public void writeLog(AppLogService_DTO appLogService_DTO)
        {

            AppLog_DTO appLog_DTO = new AppLog_DTO();
            appLog_DTO.consola = appLogService_DTO.consola;
            appLog_DTO.detalle = appLogService_DTO.detalle;
            appLog_DTO.ex = appLogService_DTO.ex;
            appLog_DTO.fecha = appLogService_DTO.fecha;
            //appLog_DTO.Logtype = appLogService_DTO.Logtype;
            appLog_DTO.ubicacion = appLogService_DTO.ubicacion;

            using (AppLog.AppLogService aals = new AppLog.AppLogService())
            {
                aals.writeLog(appLog_DTO);

            }




        }
    }
}
