﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLogService.DTO
{
    public class AppLogService_DTO
    {
       

        public Guid id { get; set; }
        //public AppLogService.LogType Logtype { get; set; }
        public string fecha { get; set; }
        public string ubicacion { get; set; }
        public string detalle { get; set; }
        public bool consola { get; set; }
        public Exception ex { get; set; }

        public AppLogService_DTO(Exception ex = null) 
        { 
            if (ex == null)
            {
                this.ex = new Exception("Excepcion nula, no aplica a operacion");
            }else
            {
                this.ex = ex;
            }
                  
        }
    }
}
