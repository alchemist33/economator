﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.DTOs
{
    public class gruposConceptos_DTO
    {
        public Guid IdGrupo { get; set; }
        public string nombreGrupo { get; set; }
    }
}
