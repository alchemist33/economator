﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.DTOs
{
    public class Presupuestos_DTO
    {
        public Guid IdPresupuesto { get; set; }
        public string nombrePresupuesto { get; set; }
        public Guid IdTipoPresupuesto { get; set; }
        public bool sumaImportes { get; set; }
    }
}
