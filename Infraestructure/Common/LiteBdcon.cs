﻿
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace LiteInfraestructure
{
    public class LiteBdCon : IDisposable
    {


        SQLiteConnection sqlite_conn;
        SQLiteConnection sqlite_Logconn;
       
        SQLiteCommand cmd = new SQLiteCommand();
       // AppLogService als; //= new AppLogService();
        //EconomatorApp.AppService appServ = new EconomatorApp.AppService();
        //AppLogService.LogType lgTyError = AppLogService.LogType.Errors;
        //AppLogService.LogType lgTyConex = AppLogService.LogType.Conections;
        public List<SQLiteConnection> listaConexiones = new List<SQLiteConnection>();


       
        public enum database
        {
            log,
            app

        }
        public enum databaseAction
        {
            Set,
            Get

        }

        public LiteBdCon()
        {
            this.listaConexiones = lstConnex();
        }

      

        public bool ConexSelector(database db, databaseAction dbAc, string query)
        {
            
            SQLiteConnection con = new SQLiteConnection();
            bool reslut = false;

            switch (db)
            {
                case database.app:
                    con = listaConexiones.Where(x => x.VfsName == "dbApp").FirstOrDefault();
                    cmd = con.CreateCommand();
                  
                    reslut = true;
                    break;

                case database.log:
                    con = listaConexiones.Where(x => x.VfsName == "dbLog").FirstOrDefault();
                    cmd = con.CreateCommand();
                  
                    reslut = true;
                    break;
            }


            switch (dbAc)
            {
                case databaseAction.Set:

                    //AppLog_DTO appLogDto = new AppLog_DTO();
                    //appLogDto.id = new Guid();
                    //appLogDto.type = AppLogService.LogType.Conections;
                    //appLogDto.detalle = "abriendo conexion con SQLite...";

                    //cmd.CommandText = appServ.LogService(appLogDto); // Escribiendo log
                    //con.Open();
                    //cmd.ExecuteNonQuery();
                    //con.Close();

                    break;

                case databaseAction.Get:
                    con.Open();

                    cmd.CommandText = query;
                    //cmd.ExecuteReader();
                    cmd.ExecuteScalar();
                    con.Close();
                    break;
            }
           
            return reslut;
        }

        public List<SQLiteConnection> lstConnex()
        {

            string dbName = "economatorDb.db";
            string LogdbName = "economatorLogDb.db";
            string currentPath;
            string connectionString;
            string connectionLogString;
            currentPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            connectionString = string.Format($"Data Source={dbName}; Version = 3; New = False; Compress = True;");
            connectionLogString = string.Format($"Data Source={LogdbName}; Version = 3; New = False; Compress = True;");
            List<SQLiteConnection> lstConnections = new List<SQLiteConnection>();
            sqlite_conn = new SQLiteConnection(connectionString);
            sqlite_Logconn = new SQLiteConnection(connectionLogString);
            sqlite_conn.VfsName = "dbApp";
            sqlite_Logconn.VfsName = "dbLog";
            lstConnections.Add(sqlite_conn);
            lstConnections.Add(sqlite_Logconn);

            // Open the connection:
            try
            {
                
                //sqlite_conn.Open();
                //sqlite_Logconn.Open();
               
                System.Console.WriteLine("Conexion SQL establecida!!");
                System.Console.WriteLine("alc.getLog()");

            }
            catch (Exception ex)
            {
                
                System.Console.WriteLine("Conexion SQL establecida!!");
               
               
            }
            return lstConnections;
        }





        public bool CreateConnection()
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            // string uri = "..\\..\\BdGlobal.db";
            string uri = "BdGlobal.db";
            bool result = false;
            // Open the connection:
            try
            {
                sqlite_conn = new SQLiteConnection($"Data Source= {uri}; Version = 3; New = True; Compress = True; ");
                sqlite_conn.Open();
                if (sqlite_conn != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {

                System.Console.WriteLine("Error Al conectar");
                System.Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

            return result;
        }

        static void CreateTable(SQLiteConnection conn)
        {

            SQLiteCommand sqlite_cmd;
            string Createsql = $@"CREATE TABLE SampleTable
               (Col1 VARCHAR(20), Col2 INT)";
            string Createsql1 = $@"CREATE TABLE SampleTable1
            (Col1 VARCHAR(20), Col2 INT)";
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = Createsql;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_cmd.CommandText = Createsql1;
            sqlite_cmd.ExecuteNonQuery();

        }

        static void InsertData(SQLiteConnection conn)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $@"INSERT INTO TEST
               (columna1, columna2) VALUES('Test Text ', 1); ";
            sqlite_cmd.ExecuteNonQuery();
            sqlite_cmd.CommandText = $@"INSERT INTO TEST
                (columna1, columna2) VALUES('Test1 Text1 ', 2); ";
            sqlite_cmd.ExecuteNonQuery();
            sqlite_cmd.CommandText = $@"INSERT INTO TEST
                (columna1, columna2) VALUES('Test2 Text2 ', 3); ";
            sqlite_cmd.ExecuteNonQuery();



            sqlite_cmd.ExecuteNonQuery();

        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
