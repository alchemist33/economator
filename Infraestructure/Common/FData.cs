﻿using Infraestructure.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Infraestructure.Common
{
    public class FData
    {
        errorCommonControl errCmCn = new errorCommonControl();
        List<Conceptos_DTO> conceptos = new List<Conceptos_DTO>();
        List<LineaCuenta_DTO> LineasCuentas = new List<LineaCuenta_DTO>();
        List<cuentas_DTO> cuentas = new List<cuentas_DTO>();
        List<DatosFuenteBankia_DTO> datosFuenteBankia = new List<DatosFuenteBankia_DTO>();
        List<gruposConceptos_DTO> grpsConcepts = new List<gruposConceptos_DTO>();
        List<Presupuestos_DTO> presupuestos = new List<Presupuestos_DTO>();
        List<TiposPresupuestos_DTO> Tipospresupuestos = new List<TiposPresupuestos_DTO>();
        public FData()
        {

        }
        public List<Conceptos_DTO> ConceptosFull()
        {
            string query = "SELECT * FROM Conceptos";
            try
            {
                using (BdCon con = new BdCon())
                {
                    conceptos = con.GetData<Conceptos_DTO>(query, null).ToList(); ;
                }
            }
            catch (Exception ex )
            {
                errCmCn.errorSelector(errorCommonControl.CustomDialog.ErrorLv2, ex);
            }
            return conceptos;
        }
        public List<cuentas_DTO> cuentasFull()
        {
            string query = "SELECT * FROM CCCs";
            try
            {
                using (BdCon con = new BdCon())
                {
                    cuentas = con.GetData<cuentas_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCmCn.errorSelector(errorCommonControl.CustomDialog.ErrorLv2, ex);
            }
            return cuentas;
        }
        public List<LineaCuenta_DTO> LineasCuentasFull()
        {
            string query = "SELECT * FROM Lineas_Cuentas";
            try
            {
                using (BdCon con = new BdCon())
                {
                    LineasCuentas = con.GetData<LineaCuenta_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCmCn.errorSelector(errorCommonControl.CustomDialog.ErrorLv2, ex);
            }
            return LineasCuentas;
        }
        public List<DatosFuenteBankia_DTO> dataBankiaFull()
        {
            string query = "SELECT * FROM datosOrigenBankia";
            try
            {
                using (BdCon con = new BdCon())
                {
                    datosFuenteBankia = con.GetData<DatosFuenteBankia_DTO>(query, null).ToList(); ;
                }
            }
            catch (Exception ex)
            {
                errCmCn.errorSelector(errorCommonControl.CustomDialog.ErrorLv2, ex);
            }
            
            return datosFuenteBankia;
        }
        public List<gruposConceptos_DTO> GruposFull()
        {
            string query = "SELECT * FROM gruposConceptos";
            try
            {
                using (BdCon con = new BdCon())
                {
                    grpsConcepts = con.GetData<gruposConceptos_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCmCn.errorSelector(errorCommonControl.CustomDialog.ErrorLv2, ex);
            }
            return grpsConcepts;
        }
        public List<Presupuestos_DTO> PresupuestosFull()
        {
            string query = "SELECT * FROM Presupuestos";
            try
            {
                using (BdCon con = new BdCon())
                {
                    presupuestos = con.GetData<Presupuestos_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCmCn.errorSelector(errorCommonControl.CustomDialog.ErrorLv2, ex);
            }
            return presupuestos;
        }
        public List<TiposPresupuestos_DTO> TipoPresupuestosFull()
        {
            string query = "SELECT * FROM TiposPresupuestos";
            try
            {
                using (BdCon con = new BdCon())
                {
                    Tipospresupuestos = con.GetData<TiposPresupuestos_DTO>(query, null).ToList();
                }
            }
            catch (Exception ex)
            {
                errCmCn.errorSelector(errorCommonControl.CustomDialog.ErrorLv2, ex);
            }
            return Tipospresupuestos;
        }
    }
    
}
