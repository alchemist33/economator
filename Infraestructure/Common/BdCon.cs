﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;


namespace Infraestructure.Common
{
    public class BdCon : IDisposable
    {
        public SqlConnection conex = new SqlConnection("server = MARK-I\\SQLEXPRESS; database=economator; integrated security = true");
        public BdCon()
        {

            conex.Open();
            LogService();
        }

        public void LogService()
        {
            //InfraestructureServiceAccess isa = new InfraestructureServiceAccess();
            //AppLogService_DTO appLogServiceDto = new AppLogService_DTO();
            //appLogServiceDto.id = Guid.NewGuid();
            ////appLogServiceDto.Logtype = AppLogService.LogType.Conections;
            //appLogServiceDto.fecha = DateTime.Now.ToString();
            //appLogServiceDto.ubicacion = System.Reflection.MethodBase.GetCurrentMethod().Name;
            //appLogServiceDto.detalle = "abriendo conexion con SQLite...";
            //isa.WriteLog(appLogServiceDto);
            //LiteInfraestructure.LiteBdCon con = new LiteInfraestructure.LiteBdCon();
            //con.ConexSelector(LiteInfraestructure.LiteBdCon.database.log, LiteInfraestructure.LiteBdCon.databaseAction.Set, query, appLogDto);
        }
        public void Dispose()
        {
            try
            {
                conex.Close();
                conex.Dispose();
            }
            catch (Exception)
            {
                throw new NotImplementedException();
            }
        }



        public void ExeQry<T>(string query, IEnumerable<T> listaObjetos)
        {
            conex.Execute(query, listaObjetos);
        }
        public IEnumerable<T> GetData<T>(string query, IEnumerable<T> listaObjetos)
        {
            IEnumerable<T> result = conex.Query<T>(query, listaObjetos);
            return result;

        }

    }
}
