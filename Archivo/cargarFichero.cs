﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorAppService;



namespace Economator
{
    public partial class cargarFichero : Form
    {
        // operaciones op = new operaciones("A2");



        public cargarFichero()
        {
            InitializeComponent();
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            ofd.ShowDialog();
            this.textBox1.Text = ofd.FileName;
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                List<string> values = new List<string>() { textBox1.Text, comboBox1.Text, comboBox3.Text, comboBox2.Text, comboBox4.Text };
                switch (comboBox2.Text.ToUpper()) 
                {
                    case "BANKIA":
                        easa.AgregarDatosOrigenBankia(values);
                        break;
                    case "ING":
                        easa.AgregarDatosOrigenING(values);
                        break;
                }
                
            }
            this.Close();
            this.Dispose();
        }
        private void ComboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            string nomLinea = comboBox3.SelectedItem.ToString();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                foreach (var reg in easa.getCuentas().Where(x => x.LineaCuenta == nomLinea).ToList())
                {
                    comboBox1.Items.Add(reg.nombreCCC);
                }
            }

        }
        private void Button3_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void comboBox2_DropDown(object sender, EventArgs e)
        {

            comboBox2.Items.Clear();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                foreach (var reg in easa.getOptionsValues("Entidades"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox2.Items.Add(reg);
                    }
                }
            }
        }

        private void comboBox3_DropDown(object sender, EventArgs e)
        {
            comboBox3.Items.Clear();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {

                foreach (var reg in easa.getOptionsValues("LineasCuentas"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox3.Items.Add(reg);
                    }
                }
            }
        }

        private void comboBox4_DropDown(object sender, EventArgs e)
        {
            comboBox4.Items.Clear();
            using (EconomatorAppServiceAccess easa = new EconomatorAppServiceAccess())
            {
                foreach (var reg in easa.getOptionsValues("TiposFichero"))
                {
                    if (!String.IsNullOrEmpty(reg))
                    {
                        comboBox4.Items.Add(reg);
                    }
                }
            }
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(comboBox2.Text))
            {
                comboBox4.Enabled = true;
            }
            else
            {
                comboBox4.Enabled = false;
            }
        }

       

        private void comboBox2_KeyUp(object sender, KeyEventArgs e)
        {
            comboBox2.Text = String.Empty;
            comboBox4.Enabled = false;
        }

        private void comboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            comboBox2.Text = String.Empty;
            comboBox4.Enabled = false;
        }

        private void comboBox4_KeyDown(object sender, KeyEventArgs e)
        {
            comboBox4.Text = String.Empty;
            comboBox3.Enabled = false;
        }

        private void comboBox4_KeyUp(object sender, KeyEventArgs e)
        {
            comboBox4.Text = String.Empty;
            comboBox3.Enabled = false;
        }

        private void comboBox4_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(comboBox4.Text))
            {
                comboBox3.Enabled = true;
            }
            else
            {
                comboBox3.Enabled = false;
            }
        }

        private void comboBox3_KeyDown(object sender, KeyEventArgs e)
        {
            comboBox3.Text = String.Empty;
            comboBox1.Enabled = false;
        }

        private void comboBox3_KeyUp(object sender, KeyEventArgs e)
        {
            comboBox3.Text = String.Empty;
            comboBox1.Enabled = false;
        }

        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(comboBox4.Text))
            {
                comboBox1.Enabled = true;
            }
            else
            {
                comboBox1.Enabled = false;
            }
        }
    }
}
