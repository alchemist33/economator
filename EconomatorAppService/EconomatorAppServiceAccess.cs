﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EconomatorApp;
using EconomatorApp.Archivo;
using EconomatorApp.Editar;
using EconomatorApp.Helper;
using EconomatorApp.startPanel;
using EconomatorAppService.DTOsTy2;
using InfraestructureService.DTOs;

namespace EconomatorAppService
{
    public class EconomatorAppServiceAccess : IDisposable
    {
        #region operaciones

        #region cuentas
        public List<EconomatorAppService.DTOs.cuentas_DTO> getCuentas()
        {
            operaciones op = new operaciones("A2");
            List<EconomatorAppService.DTOs.cuentas_DTO> lstData = new List<DTOs.cuentas_DTO>();
            foreach (var reg in op.lstCCCs)
            {
                EconomatorAppService.DTOs.cuentas_DTO data = new DTOs.cuentas_DTO();
                data.idCuenta = reg.idCuenta;
                data.nombreCCC = reg.nombreCCC;
                data.entidadCCC = reg.entidadCCC;
                data.numeroCCC = reg.numeroCCC;
                data.operativa = reg.operativa;
                data.fecha_alta = reg.fecha_alta;
                data.fecha_baja = reg.fecha_baja;
                data.LineaCuenta = reg.LineaCuenta;
                data.comentarios = reg.comentarios;

                lstData.Add(data);
            }
            return lstData;
        }
        public List<EconomatorAppService.DTOs.cuentas_DTO> getCuentas2(bool operativas = false)
        {
            operaciones op = new operaciones("A2");
            List<EconomatorAppService.DTOs.cuentas_DTO> lst = new List<EconomatorAppService.DTOs.cuentas_DTO>();
            var lst2 = op.lstCCCs;
            if (operativas)
            {
                lst2 = lst2.Where(x => x.operativa == true).ToList();
            }

            foreach (var reg in lst2)
            {

                EconomatorAppService.DTOs.cuentas_DTO ccc = new EconomatorAppService.DTOs.cuentas_DTO();
                ccc.idCuenta = reg.idCuenta;
                ccc.nombreCCC = reg.nombreCCC;
                ccc.numeroCCC = reg.numeroCCC;
                ccc.entidadCCC = reg.entidadCCC;
                ccc.fecha_alta = reg.fecha_alta;
                ccc.fecha_baja = reg.fecha_baja;
                ccc.operativa = reg.operativa;
                ccc.comentarios = reg.comentarios;
                ccc.LineaCuenta = reg.LineaCuenta;
                lst.Add(ccc);
            }
            return lst;
        }
        public List<string> getCuentasOperativas()
        {
            operaciones op = new operaciones("A2");
            List<string> lst = new List<string>();
            foreach (var reg in op.lstCCCs.Where(x => x.operativa == true))
            {
                lst.Add(reg.nombreCCC);
            }
            return lst;
        }
        #endregion
        public void AgregarDatosOrigenBankia(List<string> values)
        {
            operaciones ops = new operaciones();
            CargaFicheroCore cfc = new CargaFicheroCore();
            cfc.agregarDatosOrigenBankia(values[0], values[1], values[2], values[3], values[4]);
        }
        public void AgregarDatosOrigenING(List<string> values)
        {
            operaciones ops = new operaciones();
            CargaFicheroCore cfc = new CargaFicheroCore();
            cfc.agregarDatosOrigenING(values[0], values[1], values[2], values[3], values[4]);
        }
        public List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> getDatosFuente(DateTime desde, DateTime hasta, string cuenta)
        {
            List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> lstData = new List<DTOs.DatosFuenteBankia_DTO>();
            startPanelCore spc = new startPanelCore();
            foreach (var reg in spc.getDatosFuente(desde, hasta, cuenta))
            {
                EconomatorAppService.DTOs.DatosFuenteBankia_DTO data = new DTOs.DatosFuenteBankia_DTO();
                data.Id = reg.Id;
                data.fecha = reg.fecha;
                data.fechaValor = reg.fechaValor;
                data.descripcion = reg.descripcion;
                data.importe = reg.importe;
                data.divisaImporte = reg.divisaImporte;
                data.saldo = reg.saldo;
                data.divisaSaldo = reg.divisaSaldo;
                data.concepto1 = reg.concepto1;
                data.concepto2 = reg.concepto2;
                data.concepto3 = reg.concepto3;
                data.concepto4 = reg.concepto4;
                data.concepto5 = reg.concepto5;
                data.concepto6 = reg.concepto6;
                data.concepto7 = reg.concepto7;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;

                lstData.Add(data);
            }
            return lstData;


        }
        public List<string> getNombresTipoPresupuesto()
        {
            operaciones op = new operaciones("B1");
            return op.lstTyPresu.Select(x => x.nombreTipo).ToList(); ;
        }
        public TiposPresupuestos_DTO getTipoPresupuesto(Guid idTipo)
        {
            operaciones op = new operaciones("B1");
            var res = op.lstTyPresu.Where(x => x.IdTipoPresupuesto == idTipo).First();
            return res;
        }
        public List<TiposPresupuestos_DTO> getTiposPresupuestosFull()
        {
            operaciones op = new operaciones("B1");
            return op.lstTyPresu;
        }
        public List<Presupuestos_DTO> getPresupuestosFull()
        {
            operaciones op = new operaciones("B1");
            return op.lstPresu;
        }
        public List<string> getNombresPresupuesto()
        {
            operaciones op = new operaciones("B1");
            return op.lstPresu.Select(x => x.nombrePresupuesto).ToList(); ;
        }

        #region conceptos y grupos de conceptos
        public List<string> getDescripcionTiempo(DateTime desde, DateTime hasta)
        {
            operaciones op = new operaciones("C1");
            return op.getDescripcionTiempo(desde, hasta);
        }
        public double getGastoConceptoTiempo(DateTime desde, DateTime hasta, string descripcion)
        {
            operaciones op = new operaciones("C1");
            return op.getGastoConceptoTiempo(desde, hasta, descripcion);
        }
        public double getNumOpsConceptoTiempo(DateTime desde, DateTime hasta, string descripcion)
        {
            operaciones op = new operaciones("C1");
            return op.getNumOpsConceptoTiempo(desde, hasta, descripcion);
        }
        public List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> getListaOpsConcepto(DateTime desde, DateTime hasta, string descripcion)
        {
            operaciones op = new operaciones("C1");
            List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> lstData = new List<DTOs.DatosFuenteBankia_DTO>();

            foreach (var reg in op.getListaOpsConcepto(desde, hasta, descripcion))
            {
                EconomatorAppService.DTOs.DatosFuenteBankia_DTO data = new DTOs.DatosFuenteBankia_DTO();
                data.Id = reg.Id;
                data.fecha = reg.fecha;
                data.fechaValor = reg.fechaValor;
                data.descripcion = reg.descripcion;
                data.importe = reg.importe;
                data.divisaImporte = reg.divisaImporte;
                data.saldo = reg.saldo;
                data.divisaSaldo = reg.divisaSaldo;
                data.concepto1 = reg.concepto1;
                data.concepto2 = reg.concepto2;
                data.concepto3 = reg.concepto3;
                data.concepto4 = reg.concepto4;
                data.concepto5 = reg.concepto5;
                data.concepto6 = reg.concepto6;
                data.concepto7 = reg.concepto7;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;
                lstData.Add(data);
            }

            return lstData;
        }
        public List<string> getListGroupConcepts()
        {
            operaciones op = new operaciones("A3");
            return op.lstGrpConcepts.OrderBy(x => x.nombreGrupo).Select(x => x.nombreGrupo).ToList();
        }
        public List<string> getConcepts()
        {
            operaciones op = new operaciones("A1");
            return op.lstConcepts.Select(x => x.nombreConcepto).Distinct().ToList(); ;
        }
        public List<string> getConceptsSinGrupo()
        {
            operaciones op = new operaciones("A1");
            return op.lstConcepts.Where(x => x.grupoConcepto == null || x.grupoConcepto == String.Empty).OrderBy(x => x.nombreConcepto).Select(x => x.nombreConcepto).Distinct().ToList();
        }
        #endregion

        #endregion

        #region Opciones Core 
        public List<string> getOptionsValues(string value)
        {
            OpcionesCore optCore = new OpcionesCore();
            return optCore.getOptionValues(value);
        }
        public void updateOptionsValues(EconomatorAppService.DTOs.Opciones_DTO option)
        {
            OpcionesCore optCore = new OpcionesCore();
            InfraestructureService.DTOs.Opciones_DTO opti = new Opciones_DTO();
            optCore.updateValorOpcion(opti);
        }
        #endregion

        #region Conceptos Core
        public List<string> getConceptosGrupo(string grupo)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            var lst = editaConcpetosCore.getConceptosGrupo(grupo);
            var lstAux = lst.Select(x => x.nombreConcepto).Distinct().ToList();

            return lstAux;
        }
        public List<Conceptos_DTO> getConceptosGrupoDetail(string grupo)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            var lst = editaConcpetosCore.getConceptosGrupo(grupo);
           

            return lst;
        }
        public List<EconomatorAppService.DTOs.Conceptos_DTO> getGruposConcepto(string concepto)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            List<EconomatorAppService.DTOs.Conceptos_DTO> lstData = new List<DTOs.Conceptos_DTO>();
            var lst = editaConcpetosCore.getGruposConcepto(concepto);
            foreach (var reg in lst)
            {
                EconomatorAppService.DTOs.Conceptos_DTO data = new DTOs.Conceptos_DTO();
                data.IdConcepto = reg.IdConcepto;
                data.nombreConcepto = reg.nombreConcepto;
                data.grupoConcepto = reg.grupoConcepto;
                data.descripcion = reg.descripcion;


                lstData.Add(data);
            }

            return lstData;
        }
        public List<string> getNombreGruposConcepto(string concepto)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            var lst = editaConcpetosCore.getGruposConcepto(concepto);
            var lstAux = lst.Select(x => x.grupoConcepto).ToList();

            return lstAux;
        }
        public bool updateGrupo(string grupoNew, string grupoOld)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            return editaConcpetosCore.updateGrupo(grupoNew, grupoOld);
        }
        public void nuevoGrupo(string nombreGrupo)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            editaConcpetosCore.nuevoGrupo(nombreGrupo);
        }
        public void deleteGrupo(string nombreGrupo)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            editaConcpetosCore.deleteGrupo(nombreGrupo);
        }
        public void asignarGrupo(string concepto, string grupo)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            editaConcpetosCore.asignarGrupo(concepto, grupo);
        }
        public void quitarGrupo(string concepto, string grupo)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            editaConcpetosCore.quitarGrupo(concepto, grupo);
        }
        public void updateConceptos()
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            editaConcpetosCore.updateConceptos();
        }

        public List<string> BuscaConceptos(string busqueda)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            return editaConcpetosCore.BuscaConceptos(busqueda).Distinct().ToList();
        }
        public List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> getDetalleConceptoBankia(string concepto)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> lstData = new List<DTOs.DatosFuenteBankia_DTO>();
            var lst = editaConcpetosCore.getDetalleConceptoBankia(concepto);
            foreach (var reg in lst)
            {
                EconomatorAppService.DTOs.DatosFuenteBankia_DTO data = new DTOs.DatosFuenteBankia_DTO();
                data.Id = reg.Id;
                data.fecha = reg.fecha;
                data.fechaValor = reg.fechaValor;
                data.descripcion = reg.descripcion;
                data.importe = reg.importe;
                data.divisaImporte = reg.divisaImporte;
                data.saldo = reg.saldo;
                data.divisaSaldo = reg.divisaSaldo;
                data.concepto1 = reg.concepto1;
                data.concepto2 = reg.concepto2;
                data.concepto3 = reg.concepto3;
                data.concepto4 = reg.concepto4;
                data.concepto5 = reg.concepto5;
                data.concepto6 = reg.concepto6;
                data.concepto7 = reg.concepto7;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;


                lstData.Add(data);
            }

            return lstData;
        }
        public List<EconomatorAppService.DTOs.DatosFuenteING_DTO> getDetalleConceptoING(string concepto)
        {
            ConcpetosCore editaConcpetosCore = new ConcpetosCore();
            List<EconomatorAppService.DTOs.DatosFuenteING_DTO> lstData = new List<DTOs.DatosFuenteING_DTO>();
            var lst = editaConcpetosCore.getDetalleConceptoING(concepto);
            foreach (var reg in lst)
            {
                EconomatorAppService.DTOs.DatosFuenteING_DTO data = new DTOs.DatosFuenteING_DTO();
                data.Id = reg.Id;
                data.fechaValor = reg.fechaValor;
                data.categoria = reg.categoria;
                data.subcategoria = reg.subcategoria;
                data.descripcion = reg.descripcion;
                data.comentario = reg.comentario;
                data.imagen = reg.comentario;
                data.importe = reg.importe;
                data.saldo = reg.saldo;
                data.divisa = reg.divisa;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;


                lstData.Add(data);
            }

            return lstData;
        }

        public string getPresupuestoConcepto(string concpet)
        {
            ConcpetosCore cc = new ConcpetosCore();
            return cc.getPresupuestoConcepto(concpet);
        }

        #endregion

        #region Edita Cuentas Core
        public void nuevaCuenta(EconomatorAppService.DTOs.cuentas_DTO ccc)
        {
            InfraestructureService.DTOs.cuentas_DTO cccI = new cuentas_DTO();

            cccI.idCuenta = ccc.idCuenta;
            cccI.nombreCCC = ccc.nombreCCC;
            cccI.numeroCCC = ccc.numeroCCC;
            cccI.entidadCCC = ccc.entidadCCC;
            cccI.fecha_alta = ccc.fecha_alta;
            cccI.fecha_baja = ccc.fecha_baja;
            cccI.comentarios = ccc.comentarios;
            cccI.LineaCuenta = ccc.LineaCuenta;
            cccI.operativa = ccc.operativa;


            CuentasCore editaCuentasCore = new CuentasCore();
            editaCuentasCore.nuevaCuenta(cccI);
        }
        public void updateCuenta(EconomatorAppService.DTOs.cuentas_DTO ccc, string cuenta_old)
        {
            InfraestructureService.DTOs.cuentas_DTO cccI = new cuentas_DTO();

            cccI.idCuenta = ccc.idCuenta;
            cccI.nombreCCC = ccc.nombreCCC;
            cccI.numeroCCC = ccc.numeroCCC;
            cccI.entidadCCC = ccc.entidadCCC;
            cccI.fecha_alta = ccc.fecha_alta;
            cccI.fecha_baja = ccc.fecha_baja;
            cccI.comentarios = ccc.comentarios;
            cccI.LineaCuenta = ccc.LineaCuenta;
            cccI.operativa = ccc.operativa;


            CuentasCore editaCuentasCore = new CuentasCore();
            editaCuentasCore.updateCuenta(cccI,cuenta_old);
        }
        public void deleteCuenta(Guid idCuenta)
        {

            CuentasCore editaCuentasCore = new CuentasCore();
            editaCuentasCore.deleteCuenta(idCuenta);
        }

        #endregion

        #region Start Panel Core

        public void getDatos(bool todas, string entidad, DateTime desde, DateTime hasta, string linea, string cuenta, DataGridView datagrid)
        {
            switch (entidad)
            {
                case "BANKIA":
                    if (todas)
                    {
                        getDatosLinea(desde, hasta, linea, datagrid);
                    }
                    else
                    {
                        getDatosCuenta(desde, hasta, cuenta, datagrid);
                    }
                    break;
                case "ING":
                    if (todas)
                    {
                        getDatosLineaING(desde, hasta, linea, datagrid);
                    }
                    else
                    {
                        getDatosCuentaING(desde, hasta, cuenta, datagrid);
                    }
                    break;
            }
        }
        public List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> getDatosLinea(DateTime desde, DateTime hasta, string linea, DataGridView datagrid)
        {
            List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> lstData = new List<DTOs.DatosFuenteBankia_DTO>();

            startPanelCore spc = new startPanelCore();
            foreach (var reg in spc.getDatosFuente(desde, hasta, null, linea))
            {
                EconomatorAppService.DTOs.DatosFuenteBankia_DTO data = new DTOs.DatosFuenteBankia_DTO();
                data.Id = reg.Id;
                data.fecha = reg.fecha;
                data.fechaValor = reg.fechaValor;
                data.descripcion = reg.descripcion;
                data.importe = reg.importe;
                data.divisaImporte = reg.divisaImporte;
                data.saldo = reg.saldo;
                data.divisaSaldo = reg.divisaSaldo;
                data.concepto1 = reg.concepto1;
                data.concepto2 = reg.concepto2;
                data.concepto3 = reg.concepto3;
                data.concepto4 = reg.concepto4;
                data.concepto5 = reg.concepto5;
                data.concepto6 = reg.concepto6;
                data.concepto7 = reg.concepto7;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;
                lstData.Add(data);

            }
            datagrid.DataSource = lstData;
            return lstData;
        }

        public List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> getDatosCuenta(DateTime desde, DateTime hasta, string cuenta, DataGridView datagrid)
        {
            List<EconomatorAppService.DTOs.DatosFuenteBankia_DTO> lstData = new List<DTOs.DatosFuenteBankia_DTO>();

            startPanelCore spc = new startPanelCore();
            foreach (var reg in spc.getDatosFuente(desde, hasta, cuenta))
            {
                EconomatorAppService.DTOs.DatosFuenteBankia_DTO data = new DTOs.DatosFuenteBankia_DTO();
                data.Id = reg.Id;
                data.fecha = reg.fecha;
                data.fechaValor = reg.fechaValor;
                data.descripcion = reg.descripcion;
                data.importe = reg.importe;
                data.divisaImporte = reg.divisaImporte;
                data.saldo = reg.saldo;
                data.divisaSaldo = reg.divisaSaldo;
                data.concepto1 = reg.concepto1;
                data.concepto2 = reg.concepto2;
                data.concepto3 = reg.concepto3;
                data.concepto4 = reg.concepto4;
                data.concepto5 = reg.concepto5;
                data.concepto6 = reg.concepto6;
                data.concepto7 = reg.concepto7;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;
                lstData.Add(data);
            }
            datagrid.DataSource = lstData;
            return lstData;
        }

        public List<EconomatorAppService.DTOs.DatosFuenteING_DTO> getDatosLineaING(DateTime desde, DateTime hasta, string linea, DataGridView datagrid)
        {
            List<EconomatorAppService.DTOs.DatosFuenteING_DTO> lstData = new List<DTOs.DatosFuenteING_DTO>();

            startPanelCore spc = new startPanelCore();
            foreach (var reg in spc.getDatosFuenteING(desde, hasta, null, linea))
            {
                EconomatorAppService.DTOs.DatosFuenteING_DTO data = new DTOs.DatosFuenteING_DTO();
                data.Id = reg.Id;
                data.fechaValor = reg.fechaValor;
                data.categoria = reg.categoria;
                data.subcategoria = reg.subcategoria;
                data.descripcion = reg.descripcion;
                data.comentario = reg.comentario;
                data.imagen = reg.imagen;
                data.importe = reg.importe;
                data.saldo = reg.saldo;
                data.divisa = reg.divisa;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;
                lstData.Add(data);

            }
            datagrid.DataSource = lstData;
            return lstData;
        }

        public List<EconomatorAppService.DTOs.DatosFuenteING_DTO> getDatosCuentaING(DateTime desde, DateTime hasta, string cuenta, DataGridView datagrid)
        {
            List<EconomatorAppService.DTOs.DatosFuenteING_DTO> lstData = new List<DTOs.DatosFuenteING_DTO>();

            startPanelCore spc = new startPanelCore();
            foreach (var reg in spc.getDatosFuenteING(desde, hasta, cuenta))
            {
                EconomatorAppService.DTOs.DatosFuenteING_DTO data = new DTOs.DatosFuenteING_DTO();
                data.Id = reg.Id;
                data.fechaValor = reg.fechaValor;
                data.categoria = reg.categoria;
                data.subcategoria = reg.subcategoria;
                data.descripcion = reg.descripcion;
                data.comentario = reg.comentario;
                data.imagen = reg.imagen;
                data.importe = reg.importe;
                data.saldo = reg.saldo;
                data.divisa = reg.divisa;
                data.Entidad = reg.Entidad;
                data.LineaCuenta = reg.LineaCuenta;
                data.cccAsociada = reg.cccAsociada;
                data.TipoFichero = reg.TipoFichero;
                data.DbTimeStamp = reg.DbTimeStamp;
                lstData.Add(data);
            }
            datagrid.DataSource = lstData;
            return lstData;
        }

        #endregion

        #region graficas
        public void datosGrupos1Yr(EconomatorAppService.DTOsTy2.ChartParams_DTO cP)
        {
            graficasCore grf = new graficasCore();
            InfraestructureService.DTOsTy2.ChartParams_DTO chP = new InfraestructureService.DTOsTy2.ChartParams_DTO();
            chP.Grafica = cP.Grafica;
            chP.grpConcepts = cP.grpConcepts;
            chP.Serie = cP.Serie;
            chP.SerieName = cP.SerieName;
            chP.TypeChart = cP.TypeChart;
            chP.year = cP.year;
            chP.presupuesto = cP.presupuesto;

            grf.datosGrupos1Yr(chP);
        }
        #endregion

        #region Presupuestos Core
       
       
        public Guid getIdTipoPresu(string nomTipoPresu)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            return presuCore.getIdTipoPresu(nomTipoPresu).IdTipoPresupuesto;
        }
        public void nuevoPresupuesto(string nombrePresu, string tipoPresu, Guid idPresu,bool sumaImportes)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.nuevoPresupuesto(nombrePresu, tipoPresu, idPresu,sumaImportes);
        }
        public void updatePresupuesto(string new_nombrePresu, string old_nombrePresu, string tipoPresu, bool sumaImportes)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.updatePresupuesto(new_nombrePresu, old_nombrePresu, tipoPresu, sumaImportes);
        }
        public void deletePresupuesto(string nombrePresu)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.deletePresupuesto(nombrePresu);
        }
        public void nuevoTipoPresupuesto(string nombreTipoPresu)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.nuevoTipoPresupuesto(nombreTipoPresu);
        }
        public void updateTipoPresupuesto(string new_nombreTipoPresu, string old_nombreTipoPresu)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.updateTipoPresupuesto(new_nombreTipoPresu, old_nombreTipoPresu);
        }
        public void deleteTipoPresupuesto(string nombreTipoPresu)
        {
            operaciones op = new operaciones("B1");
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.deleteTipoPresupuesto(nombreTipoPresu);
        }
        public void nuevoImporte(string nombreTipoPresu, double cantidadImporte)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.nuevoImporte(nombreTipoPresu, cantidadImporte);
        }

        public void updateImporte(string nombreTipoPresu, double cantidadImporte, string importeN)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.updateImporte(nombreTipoPresu, cantidadImporte,importeN);
        }
        public void deleteImporte(string nombreTipoPresu, string importeN)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.deleteImporte(nombreTipoPresu, importeN);
        }


        public void asignarPresupuesto(string concpeto, string nomPresu)
        {
            PresupuestosCore presuCore = new PresupuestosCore();
            presuCore.asignarPresupuesto(concpeto, nomPresu);
        }

        #endregion

        public void Dispose()
        {

        }
    }
}
