﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EconomatorAppService.DTOs
{
    public class Opciones_DTO
    {
        public Guid idOption { get; set; }
        public string optionName { get; set; }
        public string optionValue { get; set; }

    }
}
