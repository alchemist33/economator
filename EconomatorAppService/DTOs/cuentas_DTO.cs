﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EconomatorAppService.DTOs
{
    public class cuentas_DTO
    {
        public Guid idCuenta { get; set; }
        public string LineaCuenta { get; set; }
        public string nombreCCC { get; set; }
        public string entidadCCC { get; set; }
        public string numeroCCC { get; set; }
        public bool operativa { get; set; }
        public DateTime fecha_alta { get; set; }
        public DateTime fecha_baja { get; set; }
        public string comentarios { get; set; }

    }
}
