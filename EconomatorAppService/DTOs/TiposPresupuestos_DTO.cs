﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EconomatorAppService.DTOs
{
    public class TiposPresupuestos_DTO
    {
        public Guid IdTipoPresupuesto { get; set; }
        public string nombreTipo { get; set; }
        public double importe1 { get; set; }
        public double importe2 { get; set; }
        public double importe3 { get; set; }
        public double importe4 { get; set; }
        public double importe5 { get; set; }
        public double importe6 { get; set; }
        public double importe7 { get; set; }
        public double importe8 { get; set; }
        public double importe9 { get; set; }
        public double importe10 { get; set; }
        public double importe11 { get; set; }
        public double importe12 { get; set; }
        public double importe13 { get; set; }
        public double importe14 { get; set; }
        public double importe15 { get; set; }
        public double importe16 { get; set; }
        public double importe17 { get; set; }
        public double importe18 { get; set; }
        public double importe19 { get; set; }
        public double importe20 { get; set; }
        public double importe21 { get; set; }
        public double importe22 { get; set; }
        public double importe23 { get; set; }
        public double importe24 { get; set; }
        public double importe25 { get; set; }
        public double importe26 { get; set; }
        public double importe27 { get; set; }
        public double importe28 { get; set; }
        public double importe29 { get; set; }
        public double importe30 { get; set; }
        public double importe31 { get; set; }
    }
}
