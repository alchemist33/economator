﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EconomatorAppService.DTOs
{
   public class DatosFuenteING_DTO
    {
        public Guid Id { get; set; }
        public DateTime fechaValor { get; set; }
        public string categoria { get; set; }
        public string subcategoria { get; set; }
        public string descripcion { get; set; }
        public string comentario { get; set; }
        public string imagen { get; set; }
        public double importe { get; set; }
        public double saldo { get; set; }
        public string divisa { get; set; }
        public string Entidad { get; set; }
        public string LineaCuenta { get; set; }
        public string cccAsociada { get; set; }
        public string TipoFichero { get; set; }
        public DateTime DbTimeStamp { get; set; }


    }
}
