﻿using AppLog.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppLog
{
    class errorLog : IAppLog
    {
        public enum CustomDialog
        {
            Critical,
            Error,
            ErrorLv2,
            Warning,
            Info,
        }

        public string query;

        public errorLog(AppLog_DTO appLogDto)
        {
            writeLog(appLogDto);
        }
        public void writeLog(AppLog_DTO appLogDto)
        {
            string query = $"INSERT INTO errorLog (id,type,fecha,ubicacion,detalle) VALUES ('{appLogDto.id}','{appLogDto.Logtype}','{appLogDto.fecha}','{appLogDto.ubicacion}','{appLogDto.detalle}')";

            this.query = query; 
        }

        public AppLog_DTO getLog(AppLogService.LogType logType)
        {
            throw new NotImplementedException();
        }





        private void ShowhMsg(string mensaje, string caption, MessageBoxButtons botones, MessageBoxIcon icon)
        {
            MessageBox.Show(mensaje, caption, botones, icon);
        }

        #region Niveles de detalle de error
        private string ErrorDetail1(Exception ex, string extraMsg = "")
        {
            string ret = $"Error del tipo: {ex.GetType()}.\nData: {ex.Data}.\nMensaje: {ex.Message}\n{extraMsg}";
            return ret;
        }
        private string ErrorDetail2(Exception ex, string extraMsg = "")
        {
            string ret = $"Error del tipo: {ex.GetType()}.\nData: {ex.Data}.\nMensaje: {ex.Message}.\n{extraMsg}";
            return ret;
        }
        #endregion

        public void errorSelector(CustomDialog typeSelect, Exception ex, string extraMsg = "")
        {
            switch (typeSelect)
            {
                case CustomDialog.Error:
                    ShowhMsg(ErrorDetail1(ex, extraMsg), "Error de aplicación", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case CustomDialog.ErrorLv2:
                    ShowhMsg(ErrorDetail2(ex, extraMsg), "Error de aplicación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;

            }
        }

        public string writeLog2(AppLog_DTO appLogDto)
        {
            throw new NotImplementedException();
        }
    }
}
