﻿
using AppLog.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLog
{
    public interface IAppLog
    {
        public void writeLog(AppLog_DTO appLogDto);
        public string writeLog2(AppLog_DTO appLogDto);
        public AppLog_DTO getLog(AppLogService.LogType logType);
    }
}
